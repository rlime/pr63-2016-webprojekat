﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace ApartmentsAPI.Models
{
    public class User
    {
        private string password;

        public int Id { get; set; }
        [Required(ErrorMessage = "Username is required")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Password is required")]
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = Encoder.GetSHA256(value);
            }
        }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Surname is required")]
        public string Surname { get; set; }
        public bool Blocked { get; set; } = false;
        public EGender Gender { get; set; }
        public ERole Role { get; set; }
    }
}
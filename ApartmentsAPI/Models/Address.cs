﻿namespace ApartmentsAPI.Models
{
    public class Address
    {
        public int Id { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }

        public Address() { }
        public Address(string streetAddress, string city, string postalCode, string country)
        {
            StreetAddress = streetAddress;
            City = city;
            PostalCode = postalCode;
            Country = country;
        }

        public override string ToString()
        {
            return StreetAddress + City + PostalCode + Country;
        }
    }
}
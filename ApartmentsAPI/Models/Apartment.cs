﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApartmentsAPI.Models
{
    public class Apartment
    {
        public int Id { get; set; }
        public EApartmentType Type { get; set; }
        public int RoomCount { get; set; }
        public int GuestCount { get; set; }
        public Location Location { get; set; }
        public ICollection<Reservation> Reservations { get; set; }
        public ICollection<RentDateTime> RentDates { get; set; }
        public List<RentDateTime> AvailableDates { get; set; }
        public User Host { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<Photo> Images { get; set; }
        public double PricePerNight { get; set; }
        public RentDateTime CheckInTime { get; set; }
        public RentDateTime CheckOutTime { get; set; }
        public bool IsActive { get; set; }
        public ICollection<Amenity> Amenities { get; set; }

        public Apartment()
        {
            RentDates = new List<RentDateTime>();
            Comments = new List<Comment>();
            Images = new List<Photo>();
            Amenities = new List<Amenity>();
            Reservations = new List<Reservation>();
            IsActive = false;
        }

        public double RatingAvg
        {
            get
            {
                if(Comments.Count() == 0)
                    return 0;

                double sum = 0;
                foreach(Comment c in Comments)
                {
                    sum += c.Rating;
                }

                return sum / Comments.Count();
            }
        }

        public List<RentDateTime> GetAvailableDates()
        {
            List<RentDateTime> retVal = RentDates.ToList();

            retVal.RemoveAll(r => r.Date < DateTime.Today);

            foreach (Reservation r in Reservations)
            {
                for (int i = 0; i < r.OvernightStayCount; i++)
                {
                    try
                    {
                        retVal.Remove(retVal.First(rd => rd.Date == r.StartDate.Date.AddDays(i)));
                    }
                    catch { }
                }
            }

            AvailableDates = retVal;

            return retVal;
        }
    }
}
﻿namespace ApartmentsAPI.Models
{
    public class Photo
    {
        public int Id { get; set; }
        public string Path { get; set; }

        public Photo(string path)
        {
            Path = path;
        }

        public Photo() { }
    }
}
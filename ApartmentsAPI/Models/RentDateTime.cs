﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApartmentsAPI.Models
{
    public class RentDateTime
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }

        public RentDateTime(DateTime date)
        {
            Date = new DateTime(date.Ticks);
        }
        
        public RentDateTime() { }
    }
}
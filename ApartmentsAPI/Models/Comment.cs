﻿namespace ApartmentsAPI.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public User Guest { get; set; }
        public Apartment Apartment { get; set; }
        public string Text { get; set; }
        public int Rating { get; set; }

        public Comment() { }

        public Comment(User guest, Apartment apartment, string text, int rating)
        {
            Guest = guest;
            Apartment = apartment;
            Text = text;
            Rating = rating;
        }
    }
}
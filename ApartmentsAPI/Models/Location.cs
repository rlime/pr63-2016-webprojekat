﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApartmentsAPI.Models
{
    public class Location
    {
        public int Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Address { get; set; }

        public Location() { }

        public Location(double latitude, double longitude, string address)
        {
            Latitude = latitude;
            Longitude = longitude;
            Address = address;
        }

        public override string ToString()
        {
            return Address.ToString();
        }
    }
}
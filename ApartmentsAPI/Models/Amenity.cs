﻿namespace ApartmentsAPI.Models
{
    public class Amenity
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Amenity(string name)
        {
            Name = name;
        }

        public Amenity() { }
    }
}
﻿namespace ApartmentsAPI.Models
{
    public enum ERole
    {
        Admin = 0,
        Host,
        Guest
    }
}
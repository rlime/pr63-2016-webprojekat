﻿namespace ApartmentsAPI.Models
{
    public enum EGender
    {
        Male,
        Female,
        Other
    }
}
﻿namespace ApartmentsAPI.Models
{
    public enum EReservationState
    {
        Created,
        Declined,
        Canceled,
        Accepted,
        Complete
    }
}
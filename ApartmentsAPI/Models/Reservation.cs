﻿using System;

namespace ApartmentsAPI.Models
{
    public class Reservation
    {
        public int Id { get; set; }
        public Apartment Apartment { get; set; }
        public RentDateTime StartDate { get; set; }
        public int OvernightStayCount { get; set; }
        public User Guest { get; set; }
        public EReservationState State { get; set; }

        public Reservation(int stayCount, DateTime startDate)
        {
            OvernightStayCount = stayCount;
            StartDate = new RentDateTime(startDate);
            State = EReservationState.Created;
        }

        public Reservation()
        {
            State = EReservationState.Created;
        }
    }
}
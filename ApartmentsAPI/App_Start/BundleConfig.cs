﻿using System.Web;
using System.Web.Optimization;

namespace ApartmentsAPI
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Libs/jquery/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Libs/jquery/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Libs/modernizr/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      //"~/Libs/bootstrap/bootstrap.min.js",
                      "~/Libs/bootstrap/bootstrap.bundle.min.js",
                      "~/Libs/bootstrap/bootstrap-datepicker.min.js",
                      "~/Libs/bootstrap/bootstrap-select.min.js",
                      "~/Libs/bootstrap/bootstrap-clockpicker.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Libs/bootstrap/bootstrap.min.css",
                      "~/Libs/bootstrap/bootstrap-reboot.min.css",
                      "~/Libs/bootstrap/bootstrap-grid.min.css",
                      "~/Libs/bootstrap/bootstrap-responsive.css",
                      "~/Libs/bootstrap/bootstrap-datepicker.min.css",
                      "~/Libs/bootstrap/bootstrap-select.min.css",
                      "~/Libs/bootstrap/bootstrap-clockpicker.min.css"));
        }
    }
}

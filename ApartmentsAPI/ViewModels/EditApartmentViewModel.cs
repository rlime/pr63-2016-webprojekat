﻿using ApartmentsAPI.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApartmentsAPI.ViewModels
{
    public class EditApartmentViewModel
    {
        public int Id { get; set; }
        [Required]
        public EApartmentType Type { get; set; }
        [Required(ErrorMessage = "Room Count is required")]
        public int RoomCount { get; set; }
        [Required(ErrorMessage = "Guest Count is required")]
        public int GuestCount { get; set; }
        [Required]
        public double Longitude { get; set; }
        [Required]
        public double Latitude { get; set; }
        [Required(ErrorMessage = "Location is required")]
        public string Location { get; set; }
        [Required(ErrorMessage = "Rent Dates are required")]
        public string RentDates { get; set; }
        [Required(ErrorMessage = "Photos are required")]
        public List<string> Photos { get; set; }
        [Required(ErrorMessage = "Price Per Night is required")]
        public double PricePerNight { get; set; }
        [Required(ErrorMessage = "Check In Time is required")]
        public string CheckInTime { get; set; }
        [Required(ErrorMessage = "Check Out Time is required")]
        public string CheckOutTime { get; set; }
        [Required]
        public List<string> Amenities { get; set; }
        [Required]
        public bool IsActive { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApartmentsAPI.ViewModels
{
    public class LoginUserViewModel
    {
        private string password;

        [Required(ErrorMessage = "Username is required")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Password is required")]
        public string Password
        {
            get { return password; }
            set
            {
                password = Encoder.GetSHA256(value);
            }
        }
    }
}
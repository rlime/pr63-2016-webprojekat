﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApartmentsAPI.ViewModels
{
    public class NewReservationViewModel
    {
        public int Id { get; set; }
        public List<string> ReservationDates { get; set; }
    }
}
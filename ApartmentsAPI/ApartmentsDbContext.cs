﻿using ApartmentsAPI.Models;
using SQLite.CodeFirst;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.Common;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ApartmentsAPI
{
    public class ApartmentsDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Apartment> Apartments { get; set; }

        public System.Data.Entity.DbSet<ApartmentsAPI.Models.Reservation> Reservations { get; set; }
    }
}
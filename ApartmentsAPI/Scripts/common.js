﻿let fetchAmenities = async function () {
    await fetch("/api/Amenities/", {
        method: "GET"
    })
    .then(function (response) {
        return response.json();
    })
    .then(function (response) {
        sessionStorage.setItem("amenities", response);
    })
    .catch(function (response) {
        if (response.indexOf("TypeError") < 0) {
            alert(response);
        }
    });
}

let getRole = function () {
    let url = "/api/Users/GetRole/";
    return fetch(url, {
        headers: {
            "Authorization": "Bearer " + localStorage.getItem("token")
        }
    })
    .then(function (response) {
        return response.json();
    })
    .then(function (response) {
        localStorage.setItem("role", response);
    })
    .catch(function (response) {
        if (response.toString().indexOf("TypeError") < 0) {
            alert(response);
        }
    });
}

let getUserDetails = function (id) {
    if (id) {
        let url = "/api/Users/" + id;
        return fetch(url).then(function (response) {
            return response.json();
        });
    }
    else {
        let url = "/api/users/details/";
        return fetch(url, {
            method: 'GET',
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("token")
            }
        })
        .then(function (response) {
            return response.json();
        })
        .then(function (response) {
            localStorage.setItem("username", response.Username);
            localStorage.setItem("name", response.Name);
            localStorage.setItem("surname", response.Surname);
            localStorage.setItem("gender", response.Gender);
            localStorage.setItem("banned", response.Banned);
            localStorage.setItem("role", response.Role);
        })
        .catch(function (response) {
            if (response.toString().indexOf("TypeError") < 0) {
                alert(response);
                localStorage.clear();
            }
        });
    }
};

let loadAmenities = async function () {
    let option;
    let amenities;
    await fetchAmenities();
    amenities = sessionStorage.getItem("amenities").split(",");
    return new Promise((resolve, reject) => {
        for (amenity of amenities) {
            option = '<option value="' + amenity + '">' + amenity + "</option>";
            $("#select-amenities").append(option);
        }
        resolve();
    });
};

let collectionSorter = function (collection, propCriteria, descending) {
    let sortedCollection = collection;
    if (descending) {
        sortedCollection.sort(function (a, b) {
            return ("" + b[propCriteria]).localeCompare(a[propCriteria]);
        })
    }
    else {
        sortedCollection.sort(function (a, b) {
            return ("" + a[propCriteria]).localeCompare(b[propCriteria]);
        })
    }
    return sortedCollection;
}
﻿let amenities;

$(document).ready(function () {
    if (localStorage.getItem("role") !== "0") {
        location.href = "/";
    }
    else {
        fetchAmenities().then(function () {
            amenities = sessionStorage.getItem("amenities").split(",");
            render();
        })
    }
});

let render = function () {
    $("#list").empty();

    $("#list").append(
        `<li class="list-group-item">
            <div class="input-group">
                <input id="inputAmenity" type="text" class="form-control">
                <span class="input-group-btn">
                    <button id="btnAdd" class="btn btn-default">Add</button>
                </span>
            </div>
        </li>`
    );

    amenities.forEach(function (item, idx) {
        $("#list").append(
            `<li class="list-group-item">
                ${item}
                <button data-id="${idx}" type="button" class="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </li>`
        );
    })
};

$("#list").on("click", "#btnAdd", function () {
    let textInput = $("#inputAmenity").val();
    amenities.push(textInput);
    render();
});

$("#list").on("click", ".close", function (evt) {
    var elementId = $(this).data("id");
    amenities = remove(elementId);
    render();
});

$(document).on("click", "#btn-save", function () {
    data = JSON.stringify(amenities);
    sendRequest("/Amenities/Update/", "POST", data, function () {
        $("#text-success").show("fade");
    })
});

function remove(id) {
    return amenities.filter(function (item, idx) {
        //could be object id
        return idx !== id;
    })
}
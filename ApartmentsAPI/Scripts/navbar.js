﻿$(document).ready(function () {
    if (localStorage.getItem("token")) {
        getRole().then(function () {
            role = localStorage.getItem("role");
            switch (role) {
                //Admin
                case "0":
                    $("#btn-log").text("Log Out");
                    $("#btn-reg").hide();
                    $("#a-profile").show();
                    $("#a-update-amenities").show();
                    break;
                //Host
                case "1":
                    $("#btn-log").text("Log Out");
                    $("#btn-reg").hide();
                    $("#a-profile").show();
                    $("#a-new-apartment").show();
                    break;
                //Guest
                case "2":
                    $("#btn-log").text("Log Out");
                    $("#btn-reg").hide();
                    $("#a-profile").show();
                    $("#a-reservations").show();
                    break;
                //Logged out
                default:
                    $("#btn-log").text("Log In");
                    $("#btn-reg").show();
                    $("#a-profile").hide();
                    $("#a-reservations").hide();
                    break;
            }
        });
    }
    else {
        $("#btn-log").text("Log In");
        $("#btn-reg").show();
        $("#a-profile").hide();
        $("#a-reservations").hide();
    }
});

$(document).ready(function () {
    $(document).on("click", "#btn-log", function () {
        if ($("#btn-log").text() === "Log Out") {
            localStorage.clear();
            location.reload();
        }
        else {
            location.href = "/home/login/";
        }
    });
});

$(document).ready(function () {
    $(document).on("click", "#btn-reg", function () {
        location.href = "/home/register/";
    });
});

$(document).ready(function () {
    $(document).on("click", "#a-profile", function () {
        sessionStorage.clear();
    });
});
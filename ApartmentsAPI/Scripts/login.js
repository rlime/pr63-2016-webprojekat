﻿$(document).ready(function () {
    if (localStorage.getItem("token")) {
        location.href = "/";
    }
});

let formFilled = function () {
    if ($("#Username").val().length != 0 &&
        $("#Password").val().length != 0) {
        $("#btn-login").prop("disabled", false);
        $("#text-fields-required").hide("fade");
    }
    else {
        $("#btn-login").prop("disabled", true);
        $("#text-fields-required").show("fade");
    }
};

$(document).ready(function () {
    $(document).on("keyup", ".form-control", function () {
        formFilled();
    });

    $("#Username").focus();
});

$(document).ready(function () {
    $(document).on("click", "#btn-login", function () {
        let url = window.location.origin + "/api/users/login/";
        $.ajax({
            url: url,
            method: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({
                Username: $("#Username").val(),
                Password: $("#Password").val()
            }),
            success: function (response) {
                localStorage.setItem("token", response);
                getUserDetails().then(function () {
                    location.href = "/";
                });
            },
            error: function (response) {
                let message = JSON.parse(response.responseText);
                $("#text-error").text(message.Message);
                $("#div-error").show("fade");
            }
        });
    });
});
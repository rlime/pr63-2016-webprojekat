﻿function sendRequest(url, method, data, success) {
    $.ajax({
        method: method,
        url: url,
        data: data,
        contentType: "application/json; charset=utf-8",
        headers: {
            "Authorization": "Bearer " + localStorage.getItem("token")
        },
        success: function (response) {
            success(response);
        },
        error: function (response) {
            alert(response.status + " " + response.statusText + "\n" + response.responseJSON.Message);
        }
    });
};

//function sendRequest(url, method, data) {
//    return new Promise(function (resolve, reject) {
//        $.ajax({
//            method: method,
//            url: url,
//            data: data,
//            contentType: "application/json; charset=utf-8",
//            headers: {
//                "Authorization": "Bearer " + localStorage.getItem("token")
//            },
//            success: function (response) {
//                resolve(response);
//            },
//            error: function (response) {
//                alert(response.status + " " + response.statusText);
//                reject(response);
//            }
//        });
//    });
//};
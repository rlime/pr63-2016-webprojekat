﻿let reservationValid = function () {
    let strdates = $("#ReservationDatesText").val();

    if (!strdates) {
        $("#text-date-empty").show("fade");
        return false;
    }

    strdates = strdates.split(",");

    $("#text-date-empty").hide("fade");
    if (strdates.length === 1) {
        $("#text-inconsecutive-dates").hide("fade");
        return true;
    }

    let dates = [];
    for (strdate of strdates) {
        let current = strdate.trim().split("-");

        dates.push(new Date(current[2], current[1], current[0], 15));
    }

    dates = dates.sort(function (a, b) { return b - a });

    for (let i = 0; i < dates.length - 1; i++) {
        if (dates[i] - dates[i + 1] > 86400000) {
            $("#text-inconsecutive-dates").show("fade");
            return false;
        }
    }

    $("#text-inconsecutive-dates").hide("fade");
    return true;
}

$(document).ready(function () {
    $(document).on("click", "#btn-reservation", function () {
        $("#reservation").show("fade");
        $("#btn-reservation").hide();
    });
});

$(document).ready(function () {
    $(document).on("click", "#btn-cancel-reservation", function () {
        $("#reservation").hide();
        $("#btn-reservation").show("fade");
    });
});

$(document).ready(function () {
    $(document).on("change", "#ReservationDates", function () {
        reservationValid();
    });
});

$(document).ready(function () {
    $(document).on("click", "#btn-send", function () {
        let data = '"' + localStorage.getItem("apartmentId") + "," + $("#ReservationDatesText").val() + '"';
        console.log(data);

        if (reservationValid()) {
            sendRequest("/Reservations/Add/", "POST", data, function () {
                $("#text-reservation-success").show("fade");
            })
        }
    });
});
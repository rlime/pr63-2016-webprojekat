﻿let getReservations = function () {
    let url = "/api/Reservations/";
    $("#list").empty();

    sendRequest(url, "GET", "", function (response) {
        for (reservation of response) {
            insertReservation(reservation);
        }
    });
}

let insertReservation = function (reservation) {
    let startDateSplit = reservation.StartDate.Date.slice(0, reservation.StartDate.Date.length - 9).split("-");
    let startDate = new Date(startDateSplit[0], startDateSplit[1] - 1, startDateSplit[2]);
    let state;

    switch (reservation.State) {
        case 0:
            state = "Created";
            break;
        case 1:
            state = "Rejected";
            break;
        case 2:
            state = "Canceled";
            break;
        case 3:
            state = "Accepted";
            break;
        case 4:
            state = "Finished";
            break;
        default:
            state = "Unknown";
            break;
    }

    $("#list").append(
        `
            <div class="col-12">
                <a href="/Home/ReservationDetails/${reservation.Id}" class="w-100">
                    <div class="card clickable text-light bg-secondary m-1 apartment-info rounded">
                        <div class="card-header text-light">
                            ${reservation.Apartment.Location.Address}
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-9">
                                    <ul class="text-light">
                                        <li>Guest: ${reservation.Guest.Name}</li>
                                        <li>Start Date: ${startDate.toDateString()}</li>
                                        <li>Stay Count: ${reservation.OvernightStayCount} nights</li>
                                        <li>Reservation state: ${state}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        `
    );
}
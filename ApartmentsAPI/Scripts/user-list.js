﻿let getUsers = function () {
    let url = "/api/Users/"
    $("#list").empty();

    sendRequest(url, "GET", "", function (response) {
        for (currentUser of response) {
            insertUser(currentUser);
        }
    });
}

let insertUser = function (user) {
    let gender, role;

    switch (user.Gender) {
        case 0:
            gender = "Male";
            break;
        case 1:
            gender = "Female";
            break;
        default:
            gender = "Other";
            break;
    }

    switch (user.Role) {
        case 0:
            role = "Admin";
            break;
        case 1:
            role = "Host";
            break;
        default:
            role = "Guest";
            break;
    }

    $("#list").append(
        `
            <div class="col-12">
                <a class="user-info w-100" href="/Home/UserDetails/${user.Id}">
                    <div class="card clickable text-light bg-secondary m-1 apartment-info rounded list-item">
                        <div class="card-header text-light">
                            ${user.Name} ${user.Surname}
                        </div>
                        <div class="card-body">
                            <div class="row" style="padding:10px">
                                <div class="d-flex justify-align-center ml-4" >
                                    <img src="/Image/Get?imageName=${user.Gender}.jpg" class="img-fluid d-block thumb" style="width:100px;"> 
                                </div>
                                <div class="style="margin-left:-30px; ">
                                    <ul class="text-light">
                                        <li>Username: ${user.Username}</li>
                                        <li>Gender: ${gender}</li>
                                        <li>Role: ${role}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        `
    );
    //$("#list").append(userTemplate);
    //let newId = "text-name" + currentUser.Id;
    //let newIdSelector = "#" + newId;
    //$("#text-name").attr("id", newId);
    //$(newIdSelector).text(currentUser.Name + " " + currentUser.Surname);

    //newId = "a-user" + currentUser.Id;
    //$("#a-user").attr("id", newId);
    //let url = "/Home/UserDetails/" + currentUser.Id;
    //$("#" + newId).attr("href", url);

    //newId = "img-apartment" + apartment.Id;
    //newIdSelector = "#" + newId;
    //$("#img-apartment").attr("id", newId);
    //url = "/Image/Get?imageName=" + "img.jpg"; //TODO
    //$(newIdSelector).attr("src", url);

    //newId = "text-people" + apartment.Id;
    //newIdSelector = "#" + newId;
    //$("#text-people").attr("id", newId);
    //let newText = "Number of guests allowed: " + apartment.GuestCount;
    //$(newIdSelector).text(newText);

    //newId = "text-rooms" + apartment.Id;
    //newIdSelector = "#" + newId;
    //$("#text-rooms").attr("id", newId);
    //newText = "Number of rooms: " + apartment.RoomCount;
    //$(newIdSelector).text(newText);

    //newId = "text-price" + apartment.Id;
    //newIdSelector = "#" + newId;
    //$("#text-price").attr("id", newId);
    //newText = "Price per night: " + apartment.PricePerNight + "$";
    //$(newIdSelector).text(newText);
}
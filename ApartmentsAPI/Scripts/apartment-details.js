﻿let lon;
let lat;

// Inserts image in gallery with the correct image url
//
let insertPhoto = function (photo) {
    let id = photo.slice(0, photo.indexOf("."));

    $("#gallery").append(
        `
        <div class="col-3 thumb" id="div-${id}">
            <span class="closes" id="${id}" title="Delete">&times;</span>
            <img src="/Image/Get?imageName=${photo}" class="zoom img-fluid" style="margin-bottom:15px">
        </div>
    `);
}

function simpleSort(a, b) {
    return ("" + a).localeCompare("" + b);
}

// Gets Apartment details and saves them in localStorage
// Returns a promise
// Redirects to home if no ID is passed as arguement
//
let getApartmentDetails = function (id) {
    if (id) {
        let url = "/api/Apartments/" + id;
        return fetch(url).then(function (response) {
            return response.json();
        })
            .then(function (response) {
                let rentDates = collectionSorter(response.RentDates, "Date").map(rd => rd.Date.slice(0, rd.Date.length - 9));
                let availableDates = response.AvailableDates.map(a => a.Date.slice(0, a.Date.length - 9));

                let checkInTime = response.CheckInTime.Date.slice(response.CheckInTime.Date.indexOf("T") + 1, response.CheckInTime.Date.length - 3);
                let checkOutTime = response.CheckOutTime.Date.slice(response.CheckOutTime.Date.indexOf("T") + 1, response.CheckOutTime.Date.length - 3);

                let amenityNames = response.Amenities.map(a => a.Name);
                let photoPaths = response.Images.map(p => p.Path);

                localStorage.setItem("isActive", response.IsActive);
                localStorage.setItem("type", response.Type);
                localStorage.setItem("rooms", response.RoomCount);
                localStorage.setItem("guests", response.GuestCount);
                localStorage.setItem("location", response.Location.Address);
                localStorage.setItem("longitude", response.Location.Longitude);
                localStorage.setItem("latitude", response.Location.Latitude);
                localStorage.setItem("rentDates", rentDates);
                localStorage.setItem("availableDates", availableDates);     
                localStorage.setItem("photos", photoPaths);
                localStorage.setItem("price", response.PricePerNight);
                localStorage.setItem("checkInTime", checkInTime);
                localStorage.setItem("checkOutTime", checkOutTime);
                localStorage.setItem("apartmentAmenities", amenityNames);
            })
            .catch(function (response) {
                if (Object.prototype.toString.call(response) == '[object String]') {
                    if (response.indexOf("TypeError") < 0) {
                        alert(response);
                    }
                }
            })
    }
    else {
        location.href = "/";
    }
};

// Initializes Bootstrap Selectpicker, Clockpicker and Datepicker
//
let initEditApartmentPickers = async function () {
    loadAmenities().then(function () {
        $("#select-amenities").val(localStorage.getItem("apartmentAmenities").split(","))
        $("#select-amenities").selectpicker("refresh");
    });

    let dateStrings = localStorage.getItem("rentDates").split(",");
    let dates = [];

    for (d of dateStrings) {
        dateSplit = d.split("-");
        dates.push(new Date(dateSplit[0], (parseInt(dateSplit[1]) - 1).toString(), dateSplit[2]));
    }

    let firstTime = true;

    $("#RentDates")
        //    .on("show", function () {
        //    if (!firstTime) {
        //        return;
        //    }

        //        firstTime = false;

        //    for (d of dates) {
        //        $(".datepicker-days > table > tbody > tr > td.day").each(function () {
        //            let tmp = new Date(1970, 0, 1);
        //            tmp.setUTCSeconds(Number.parseInt($(this).attr("data-date")) / 1000);

        //            if (tmp.getDate() === d.getDate() && tmp.getMonth() === d.getMonth() && tmp.getFullYear() === d.getFullYear()) {
        //                if ($("#RentDatesText").val() === "") {
        //                    $("#RentDatesText").val(d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear())
        //                }
        //                else {
        //                    $("#RentDatesText").val($("#RentDatesText").val() + ", " + d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear())
        //                }

        //                $(this).addClass("active");
        //                return;
        //            }
        //        });
        //    }
        //})
        .datepicker({
            multidate: true,
            format: "dd-m-yyyy",
            clearBtn: true,
            multidateSeparator: ", ",
            weekStart: "1",
            startDate: "+0d"
        }).datepicker("setDate", dates);

    $('.clockpicker').clockpicker({
        donetext: "Done",
        placement: "top",
        autoclose: "true"
    });

    $('#ReservationDates').datepicker({
        multidate: true,
        format: "dd-mm-yyyy",
        clearBtn: true,
        multidateSeparator: ", ",
        beforeShowDay: function (date) {        // Date filter
            let available = [];
            available = localStorage.getItem("availableDates").split(",");
            for (let i = 0; i < available.length; i++) {
                available[i] = available[i].trim();
                let dateSplit = available[i].split("-")
                let current = new Date(dateSplit[0], dateSplit[1] - 1, dateSplit[2]);
                if (date.getFullYear() === current.getFullYear() &&
                    date.getMonth() === current.getMonth() &&
                    date.getDate() === current.getDate()) {
                    return true;
                }
            }
            return false;
        },
        weekStart: "1"
    });

    lon = localStorage.getItem("longitude");
    lat = localStorage.getItem("latitude");
}

let loadMap = function () {
    let attribution = new ol.control.Attribution({
        collapsible: false
    });

    let map = new ol.Map({
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            })
        ],
        target: 'map',
        view: new ol.View({
            center: [0, 0],
            zoom: 2
        })
    });

    let reverseGeocoding = function (lon, lat) {
        fetch("http://nominatim.openstreetmap.org/reverse?format=json&zoom=17&lon=" + lon + "&lat=" + lat).then(function (response) {
            return response.json();
        }).then(function (json) {
            $("#Location").val(json.display_name);
        })
    }
    map.on("click", function (e) {
        let coordinate = ol.proj.toLonLat(e.coordinate).map(function (val) {
            return val.toFixed(6);
        });
        lon = coordinate[0];
        lat = coordinate[1];
        reverseGeocoding(lon, lat);
        $("#modal-map").modal("hide");
    });
}

$(document).on("shown.bs.modal", "#modal-map", function () {
    $("#map").empty();
    //timeout because of modal fade
    setTimeout(loadMap(), 400);
})

$(document).ready(function () {
    let urlParts = location.href.split("/");
    localStorage.setItem("apartmentId", urlParts[urlParts.length - 1]);

    getApartmentDetails(localStorage.getItem("apartmentId")).then(function () {
        initEditApartmentPickers();

        if (localStorage.getItem("role") === "1" || localStorage.getItem("role") === "0") {
            $("#btn-reservation").hide();

            let apartment = {
                isActive: localStorage.getItem("isActive"),
                type: localStorage.getItem("type"),
                rooms: localStorage.getItem("rooms"),
                guests: localStorage.getItem("guests"),
                location: localStorage.getItem("location"),
                rentDates: localStorage.getItem("rentDates"),
                photos: localStorage.getItem("photos"),
                price: localStorage.getItem("price"),
                checkInTime: localStorage.getItem("checkInTime"),
                checkOutTime: localStorage.getItem("checkOutTime"),
                amenities: localStorage.getItem("apartmentAmenities")
            };

            for (photo of apartment.photos.split(',')) {
                insertPhoto(photo);
            }

            $(".closes").hide();

            if (apartment.isActive === "true") {
                $("#dd-is-active").text("Active");
                $("#IsActive").val(1);
            }
            else {
                $("#dd-is-active").text("Inactive");
                $("#IsActive").val(0);
            }

            switch (apartment.type) {
                case "0":
                    $("#dd-type").text("Full Apartment");
                    break;
                case "1":
                    $("#dd-type").text("Room");
                    break;
                default:
                    $("#dd-type").text("Undefined");
                    break;
            }

            $("#dd-rooms").text(apartment.rooms);
            $("#dd-guests").text(apartment.guests);
            $("#dd-location").text(apartment.location);
            $("#dd-rent-dates").text(apartment.rentDates);
            $("#dd-price").text(apartment.price + "$");
            $("#dd-check-in-time").text(apartment.checkInTime);
            $("#dd-check-out-time").text(apartment.checkOutTime);
            $("#dd-amenities").text(apartment.amenities);

            $("#Type").val(apartment.type);
            $("#RoomCount").val(apartment.rooms);
            $("#GuestCount").val(apartment.guests);
            $("#Location").val(apartment.location);
            $("#PricePerNight").val(apartment.price);
            $("#CheckInTime").val(apartment.checkInTime);
            $("#CheckOutTime").val(apartment.checkOutTime);

            //let text;
            //for (photo of apartment.photos) {
            //    if (text.length !== 0) {

            //    }
            //    text = $("#text-filename").text();
            //}

            //$("#Amenities").val(apartment.amenities);
        }
        else {
            if (localStorage.getItem("role") === "2") {
                $("#btn-reservation").show();
            }
            else {
                $("#btn-reservation").hide();
            }

            $("#btn-edit").hide();

            let apartment = {
                isActive: localStorage.getItem("isActive"),
                type: localStorage.getItem("type"),
                rooms: localStorage.getItem("rooms"),
                guests: localStorage.getItem("guests"),
                location: localStorage.getItem("location"),
                rentDates: localStorage.getItem("rentDates"),
                availableDates: localStorage.getItem("availableDates"),
                photos: localStorage.getItem("photos"),
                price: localStorage.getItem("price"),
                checkInTime: localStorage.getItem("checkInTime"),
                checkOutTime: localStorage.getItem("checkOutTime"),
                amenities: localStorage.getItem("apartmentAmenities")
            };

            for (photo of apartment.photos.split(',')) {
                insertPhoto(photo);
            }

            $(".closes").hide();

            if (apartment.isActive === "true") {
                $("#dd-is-active").text("Active");
                $("#IsActive").val(1);
            }
            else {
                $("#dd-is-active").text("Inactive");
                $("#IsActive").val(0);
            }

            switch (apartment.type) {
                case "0":
                    $("#dd-type").text("Full Apartment");
                    break;
                case "1":
                    $("#dd-type").text("Room");
                    break;
                default:
                    $("#dd-type").text("Undefined");
                    break;
            }

            $("#dd-rooms").text(apartment.rooms);
            $("#dd-guests").text(apartment.guests);
            $("#dd-location").text(apartment.location);
            $("#dd-rent-dates").text(apartment.availableDates);
            $("#dd-price").text(apartment.price + "$");
            $("#dd-check-in-time").text(apartment.checkInTime);
            $("#dd-check-out-time").text(apartment.checkOutTime);
            $("#dd-amenities").text(apartment.amenities);
        }
    });
});

$(document).ready(function () {
    $(document).on("click", ".closes", function () {
        let selector = "#div-" + $(this).attr("id");
        $(selector).hide("fade");

        let urlParts = $(selector).children("img").first().attr("src").split("/");
        let filename = urlParts[urlParts.length - 1].split("=")[1];


        let filenames = localStorage.getItem("photos").split(",");
        let i = filenames.findIndex(f => f == filename);


        if (i >= 0) {
            filenames.splice(i, 1);
        }
        console.log(filenames);

        localStorage.setItem("photos", filenames);
    })
})

let files;

$(document).ready(function () {
    $("#input-file").on("change", function () {
        files = $("#input-file").prop("files");
        let names = $.map(files, function (val) { return val.name; });
        $("#text-filename").empty();
        for (fileName of names) {
            let text = $("#text-filename").text();
            if (text.length >= 40) {
                text = text.slice(0, 40);
                text = text + "...";
                $("#text-filename").text(text);
                break;
            }
            text = text + fileName + ", ";
            $("#text-filename").text(text);
        }

        $("#Photos").val($("#text-filename").text());
    });
});

$(document).ready(function () {
    $(document).on("click", "#upload", function () {
        let formData = new FormData();

        for (file of files) {
            formData.append("file", file, file.name);
            formData.append("upload_file", true);
        }

        $.ajax({
            type: "POST",
            url: "/Image/Upload",
            success: function (response) {
                let filenames;

                if (localStorage.getItem("photos") === "") {
                    filenames = [];
                }
                else {
                    filenames = localStorage.getItem("photos").split(",");
                }

                console.log(filenames);
                for (filename of response) {
                    filenames.push(filename);
                    insertPhoto(filename);
                }

                localStorage.setItem("photos", filenames)
                

                $("#text-upload-success").text("Succesfully uploaded!");
                $("#text-upload-success").show("fade");
            },
            error: function (response) {
                $("#text-upload-error").text(response.statusText);
                $("#text-upload-error").show("fade");
            },
            async: true,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            timeout: 60000
        });
    })
});

$(document).ready(function () {
    $(document).on("click", "#btn-edit", function () {
        $("#dl").hide();
        $("#form").show();
        $(".closes").show();
    });
});

$(document).ready(function () {
    $(document).on("click", "#btn-cancel", function () {
        $(".closes").hide();
        $("#form").hide();
        $("#dl").show();
    });
});

$(document).ready(function () {
    $(document).on("click", "#btn-save", function () {
        let active;

        if ($("#IsActive").val() == 0) {
            active = false;
        }
        else {
            active = true;
        }

        let url = "/api/Apartments/Edit"
        let data = JSON.stringify({
            Id: localStorage.getItem("apartmentId"),
            IsActive: active,
            Type: $("#Type").val(),
            RoomCount: $("#RoomCount").val(),
            GuestCount: $("#GuestCount").val(),
            Location: $("#Location").val(),
            Longitude: Number(lon),
            Latitude: Number(lat),
            RentDates: $("#RentDatesText").val(),
            Photos: localStorage.getItem("photos").split(","),
            PricePerNight: $("#PricePerNight").val(),
            CheckInTime: $("#CheckInTime").val(),
            CheckOutTime: $("#CheckOutTime").val(),
            Amenities: $("#select-amenities").val()
        });

        sendRequest(url, "PUT", data, function (response) {
            getApartmentDetails(localStorage.getItem("apartmentId")).then(function () {
                location.reload();
            });
        });
    });
});
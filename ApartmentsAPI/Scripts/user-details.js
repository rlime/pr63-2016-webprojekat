﻿let userId;

$(document).ready(function () {
    let urlParts = location.href.split("/");
    userId = urlParts[urlParts.length - 1];

    if (!localStorage.getItem("token")) {
        location.href = "/home/login/";
        return;
    }

    else if (localStorage.getItem("role") !== "0") {      //Specific user details (only admins)
        $("#dt-banned").hide();
        $("#dd-banned").hide();
        $("#div-role").hide();
    }

    if (Number.isNaN(userId) || userId === "") {
        url = "/api/Users/GetId";
        fetch(url, {
            method: 'GET',
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("token")
            }
        })
            .then(function (response) {
                return response.json()
            })
            .then(function (response) {
                userId = response;

                let promise = getUserDetails(userId);

                promise.then(function (user) {
                    $("#dd-username").text(user.Username);
                    $("#dd-name").text(user.Name);
                    $("#dd-surname").text(user.Surname);
                    switch (user.Gender) {
                        case 0:
                            $("#dd-gender").text("Male");
                            break;
                        case 1:
                            $("#dd-gender").text("Female");
                            break;
                        case 2:
                            $("#dd-gender").text("Other");
                            break;
                        default:
                            $("#dd-gender").text("Undefined");
                            break;
                    }
                    switch (user.Role) {
                        case 0:
                            $("#dd-role").text("Admin");
                            break;
                        case 1:
                            $("#dd-role").text("Host");
                            break;
                        case 2:
                            $("#dd-role").text("Guest");
                            break;
                        default:
                            $("#dd-role").text("Undefined");
                            break;
                    }
                    switch (user.Banned) {
                        case 0:
                            $("#dd-banned").text("False");
                            break;
                        case 1:
                            $("#dd-banned").text("True");
                            break;
                        default:
                            $("#dd-banned").text("Undefined");
                            break;
                    }

                    $("#Username").val(user.Username);
                    $("#Name").val(user.Name);
                    $("#Surname").val(user.Surname);
                    $("#Gender").val(user.Gender);

                    if (user.Role !== 0) {
                        $("#Role").prop("disabled", false);
                    }
                    $("#Role").val(user.Role);
                })
            })
            .catch(function (response) {
                if (response.indexOf("TypeError") < 0) {
                    alert(response);
                }
            });
    }
    else {
        let promise = getUserDetails(userId);

        promise.then(function (user) {
            $("#dd-username").text(user.Username);
            $("#dd-name").text(user.Name);
            $("#dd-surname").text(user.Surname);
            switch (user.Gender) {
                case 0:
                    $("#dd-gender").text("Male");
                    break;
                case 1:
                    $("#dd-gender").text("Female");
                    break;
                case 2:
                    $("#dd-gender").text("Other");
                    break;
                default:
                    $("#dd-gender").text("Undefined");
                    break;
            }
            switch (user.Role) {
                case 0:
                    $("#dd-role").text("Admin");
                    break;
                case 1:
                    $("#dd-role").text("Host");
                    break;
                case 2:
                    $("#dd-role").text("Guest");
                    break;
                default:
                    $("#dd-role").text("Undefined");
                    break;
            }
            switch (user.Banned) {
                case 0:
                    $("#dd-banned").text("False");
                    break;
                case 1:
                    $("#dd-banned").text("True");
                    break;
                default:
                    $("#dd-banned").text("Undefined");
                    break;
            }

            $("#Username").val(user.Username);
            $("#Name").val(user.Name);
            $("#Surname").val(user.Surname);
            $("#Gender").val(user.Gender);
            $("#Role").val(user.Role);

            if (user.Role !== 0) {
                $("#Role").prop("disabled", false);
            }
        })
        .catch(function (response) {
            if (response.indexOf("TypeError") < 0) {
                alert(response);
            }
        });
    }
});

$(document).ready(function () {
    $(document).on("click", "#btn-edit", function () {
        $("#dl").hide();
        formFilled();
        $("#form").show();
    });
});

$(document).ready(function () {
    $(document).on("click", "#btn-cancel", function () {
        $("#form").hide();
        $("#dl").show();
    });
});

$(document).ready(function () {
    $(document).on("click", "#btn-save", function () {
        let role = $("#Role").val();

        if ($("#Role").prop("disabled")) {
            role = 0;
        }
        
        let url = "/api/Users/Edit"
        let data = JSON.stringify({
            Id: userId,
            Username: $("#Username").val(),
            Password: $("#Password").val(),
            ConfirmPassword: $("#ConfirmPassword").val(),
            Name: $("#Name").val(),
            Surname: $("#Surname").val(),
            Gender: $("#Gender").val(),
            Role: role
        });
        sendRequest(url, "PUT", data, function (response) {
            if (userId === localStorage.getItem("id")) {
                localStorage.setItem("token", response);
            }

            getUserDetails().then( function () {
                location.reload();
            });
        });
    });
});

//
// VALIDATION
//

let usernameCondition = true;
let passwordCondition = true;
let passwordConfirmCondition = true;
let passwordMinLength = 6;

let formValid = function () {
    if (formFilled() &&
        usernameCondition &&
        passwordCondition &&
        passwordConfirmCondition)
        $("#btn-save").prop("disabled", false);
    else
        $("#btn-save").prop("disabled", true);
}

let formFilled = function () {
    if (($("#Username").val().length == 0 ) ||
        ($("#Name").val().length == 0 ) ||
        ($("#Surname").val().length == 0)) {
        $("#text-fields-required").show("fade");
        return false;
    }
    else {
        $("#text-fields-required").hide("fade");
        return true;
    }
}

// Does the username already exist?
$(document).ready(function () {
    $(document).on("keyup", "#Username", function () {
        let usrnm = $("#Username").val();
        if (usrnm.toLowerCase() != localStorage.getItem("username").toLowerCase()) {
            $.ajax({
                url: "/api/users/checkusername/",
                method: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    Username: $("#Username").val(),
                    Password: $("#Password").val(),
                    ConfirmPassword: $("#ConfirmPassword").val(),
                    Name: $("#Name").val(),
                    Surname: $("#Surname").val(),
                    Gender: $("#Gender").val()
                }),
                success: function (response) {
                    if (response) {
                        //username free
                        usernameCondition = true;
                        $("#text-username-taken").hide("fade");
                        formValid();
                    }
                    else {
                        //username taken
                        usernameCondition = false;
                        $("#btn-save").prop("disabled", true);
                        $("#text-username-taken").show("fade");
                    }
                }
            });
        }
    });
});

// Is password longer than 6?
$(document).ready(function () {
    $(document).on("keyup", "#Password", function () {
        if ($("#Password").val().length < passwordMinLength &&
            $("#Password").val().length != 0) {
            $("#btn-save").prop("disabled", true);
            $("#text-password-short").show("fade");
            passwordCondition = false;
        }
        else {
            $("#text-password-short").hide("fade");
            passwordCondition = true;
            formValid();
        }
    });
});

// Is password confirmation correct?
function testPasswords() {
    if ($("#Password").val() === $("#ConfirmPassword").val()) {
        $("#text-password-different").hide("fade");
        passwordConfirmCondition = true;
        formValid();
    }
    else {
        $("#btn-register").prop("disabled", true);
        $("#text-password-different").show("fade");
        passwordConfirmCondition = false;
    }
}

$(document).ready(function () {
    $(document).on("keyup", "#ConfirmPassword", function () {
        testPasswords();
    });
    $(document).on("keyup", "#Password", function () {
        testPasswords();
    });
});

$(document).ready(function () {
    $(document).on("keyup", ".form-control", function () {
        formValid();
    });
});

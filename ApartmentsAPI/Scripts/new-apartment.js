﻿if (localStorage.getItem("role") !== "1") {
    location.href = "/"
}

let files;

$(document).on("shown.bs.modal", "#modal-map", function () {
    $("#map").empty();
    //timeout because of modal fade
    setTimeout(loadMap(), 400);
})

let initNewApartmentPickers = function () {
    loadAmenities().then(function () {
        $("#select-amenities").val(localStorage.getItem("apartmentAmenities").split(","))
        $("#select-amenities").selectpicker("refresh");
    });

    $('#RentDates').datepicker({
        multidate: true,
        format: "dd/mm/yyyy",
        clearBtn: true,
        multidateSeparator: ", ",
        startDate: "+0d",
        weekStart: "1",
    });

    $('.clockpicker').clockpicker({
        donetext: "Done",
        placement: "top",
        autoclose: "true"
    });
}

$(document).ready(initNewApartmentPickers());

$(document).ready(function () {
    $("#input-file").on("change", function () {
        files = $("#input-file").prop("files");
        let names = $.map(files, function (val) { return val.name; });
        $("#text-filename").empty();
        for (fileName of names) {
            let text = $("#text-filename").text();
            if (text.length >= 40) {
                text = text.slice(0, 40);
                text = text + "...";
                $("#text-filename").text(text);
                break;
            }
            text = text + fileName + ", ";
            $("#text-filename").text(text);
        }

        $("#Photos").val($("#text-filename").text());
    });
});

let filenames;

$(document).ready(function () {
    $(document).on("click", "#upload", function () {
        let formData = new FormData();

        for (file of files) {
            formData.append("file", file, file.name);
            formData.append("upload_file", true);
        }

        $.ajax({
            type: "POST",
            url: "/Image/Upload",
            success: function (response) {
                filenames = response;
                $("#text-upload-success").text("Succesfully uploaded!");
                $("#text-upload-success").show("fade");
            },
            error: function (response) {
                $("#text-upload-error").text(response.statusText);
                $("#text-upload-error").show("fade");
            },
            async: true,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            timeout: 60000
        });
    })
});

let lon;
let lat;

let loadMap = function () {
    let attribution = new ol.control.Attribution({
        collapsible: false
    });

    let map = new ol.Map({
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            })
        ],
        target: 'map',
        view: new ol.View({
            center: [0, 0],
            zoom: 2
        })
    });

    let reverseGeocoding = function (lon, lat) {
        fetch("http://nominatim.openstreetmap.org/reverse?format=json&zoom=17&lon=" + lon + "&lat=" + lat).then(function (response) {
            return response.json();
        }).then(function (json) {
            $("#Location").val(json.display_name);
        })
    }
    map.on("click", function (e) {
        let coordinate = ol.proj.toLonLat(e.coordinate).map(function (val) {
            return val.toFixed(6);
        });
        lon = coordinate[0];
        lat = coordinate[1];
        reverseGeocoding(lon, lat);
        $("#modal-map").modal("hide");
    });
}

//
// VALIDATION
//

let roomCondition = false;
let guestCondition = false;
let priceCondition = false;

let formFilled = function () {
    if ($("#RoomCount").val().length != 0 &&
        $("#GuestCount").val().length != 0 &&
        $("#Location").val().length != 0 &&
        $("#RentDatesText").val().length != 0 &&
        $("#PricePerNight").val().length != 0 &&
        $("#CheckInTime").val().length != 0 &&
        $("#CheckOutTime").val().length != 0 &&
        $("#select-amenities").val().length != 0 &&
        $("#text-upload-success").text().length != 0) {
        $("#text-fields-required").hide("fade");
        return true;
    }
    else {
        $("#text-fields-required").show("fade");
        return false;
    }
}

let formValid = function () {
    return formFilled() && roomCondition && guestCondition && priceCondition;
}

$(document).ready(function () {
    $(document).on("change", "#Type", function () {
        if ($("#Type").val() === "0") {
            $("#RoomCount").prop("disabled", false);
        }
        else {
            $("#RoomCount").val(1);
            $("#RoomCount").prop("disabled", true);
            $("#RoomCount").trigger("change");
        }
    });
});

$(document).ready(function () {
    $(document).on("change", "#RoomCount", function () {
        let value = $("#RoomCount").val();

        if (value <= 0) {
            $("#number-rooms-negative").show("fade");
            roomCondition = false;
            return;
        }
        else {
            $("#number-rooms-negative").hide("fade");
            roomCondition = true;
        }

        if ((value % 1) !== 0) {
            $("#number-rooms-decimal").show("fade");
            roomCondition = false;
        }
        else {
            $("#number-rooms-decimal").hide("fade");
            roomCondition = true;
        }
    });
});

$(document).ready(function () {
    $(document).on("change", "#GuestCount", function () {
        let value = $("#GuestCount").val();

        if (value <= 0) {
            $("#number-guests-negative").show("fade");
            guestCondition = false;
            return;
        }
        else {
            $("#number-guests-negative").hide("fade");
            guestCondition = true;
        }

        if ((value % 1) !== 0) {
            $("#number-guests-decimal").show("fade");
            guestCondition = false;
        }
        else {
            $("#number-guests-decimal").hide("fade");
            guestCondition = true;
        }
    });
});

$(document).ready(function () {
    $(document).on("change", "#PricePerNight", function () {
        let value = $("#PricePerNight").val();

        if (value <= 0) {
            $("#number-price-negative").show("fade");
            priceCondition = false;
            return;
        }
        else {
            $("#number-price-negative").hide("fade");
            priceCondition = true;
        }
    });
});

$(document).ready(function () {
    $(document).on("click", "#btn-add", function () {
        if (formValid()) {            
            $.ajax({
                url: "/api/apartments/add/",
                method: "POST",
                contentType: "application/json; charset=utf-8",
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem("token")
                },
                data: JSON.stringify({
                    Type: $("#Type").val(),
                    RoomCount: $("#RoomCount").val(),
                    GuestCount: $("#GuestCount").val(),
                    Longitude: lon,
                    Latitude: lat,
                    Location: $("#Location").val(),
                    RentDates: $("#RentDatesText").val(),
                    Photos: filenames,
                    PricePerNight: $("#PricePerNight").val(),
                    CheckInTime: $("#CheckInTime").val(),
                    CheckOutTime: $("#CheckOutTime").val(),
                    Amenities: $("#select-amenities").val()
                }),
                success: function () {
                    $("#text-success").show("fade");
                    let n = $(document).height();
                    $("html, body").animate({ scrollTop: n }, 50);

                    setTimeout(function () {
                        $("#text-success").hide("fade");
                    }, 2000);
                },
                error: function (response) {
                    $("#text-error").text(response.statusText);
                    $("#text-error").show("fade");
                }
            });
        }
    });
});
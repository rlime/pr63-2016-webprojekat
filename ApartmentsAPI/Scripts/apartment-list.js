﻿let getApartments = function () {
    let url = "/api/Apartments/";
    $("#list").empty();

    sendRequest(url, "GET", "", function (response) {
        let apartments = collectionSorter(response, "PricePerNight", true);

        for (apartment of apartments) {
            insertApartment(apartment);
        }
    });
}

let insertApartment = function (apartment) {

    $("#list").append(
        `
        <div class="w-100 h-100">
            <div class="col-md-12 d-flex justify-content-start align-items-center flex-column" >
                <a href="/Home/ApartmentDetails/${apartment.Id}">
                    <div class="card clickable text-light bg-secondary m-1 apartment-info rounded list-item">
                        <div class="card-header text-light">
                            ${apartment.Location.Address}
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="/Image/Get?imageName=${apartment.Images[0].Path}" class="img-fluid d-block thumb"> 
                                </div>
                                <div class="col-md-9">
                                    <ul class="text-light">
                                        <li>Number of guests: ${apartment.GuestCount}</li>
                                        <li>Number of rooms: ${apartment.RoomCount}</li>
                                        <li>Price per night: ${apartment.PricePerNight}$</li>
                                        <li>Amenities: 
                                            ${
                                                function () {
                                                    let retVal = "";
                                                    for (amenity of apartment.Amenities) {
                                                        retVal += amenity.Name + ", ";
                                                    }

                                                    return retVal.slice(0, retVal.length - 2);
                                                }()
                                            }
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    `
    );
}

let loadMap = function () {
    let attribution = new ol.control.Attribution({
        collapsible: false
    });

    let map = new ol.Map({
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            })
        ],
        target: 'map',
        view: new ol.View({
            center: [0, 0],
            zoom: 2
        })
    });

    let reverseGeocoding = function (lon, lat) {
        fetch("http://nominatim.openstreetmap.org/reverse?format=json&zoom=10&lon=" + lon + "&lat=" + lat).then(function (response) {
            return response.json();
        }).then(function (json) {
            $("#Location").val(json.display_name);
        })
    }
    map.on("click", function (e) {
        let coordinate = ol.proj.toLonLat(e.coordinate).map(function (val) {
            return val.toFixed(6);
        });
        let lon = coordinate[0];
        let lat = coordinate[1];
        reverseGeocoding(lon, lat);
        $("#modal-map").modal("hide");
    });
}

$(document).on("shown.bs.modal", "#modal-map", function () {
    $("#map").empty();
    //timeout because of modal fade
    setTimeout(loadMap(), 400);
})
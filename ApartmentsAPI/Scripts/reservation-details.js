﻿let state;
let startDate;
let stayCount;

let getReservationDetails = function (id) {
    if (id) {
        let url = "/api/Reservations/" + id;
        return fetch(url).then(function (response) {
            return response.json();
        })
            .then(function (response) {
                let dateSplit = response.StartDate.Date.slice(0, response.StartDate.Date.length - 9).split("-");
                let date = new Date(dateSplit[0], dateSplit[1] - 1, dateSplit[2]);

                $("#dd-guest").text(response.Guest.Username);
                $("#dd-apartment").text(response.Apartment.Location.Address);
                $("#dd-startdate").text(date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear());
                $("#dd-overnightstaycount").text(response.OvernightStayCount + " nights");

                switch (response.State) {
                    case 0:
                        $("#dd-state").text("Created");
                        break;
                    case 1:
                        $("#dd-state").text("Declined");
                        break;
                    case 2:
                        $("#dd-state").text("Cancelled");
                        break;
                    case 3:
                        $("#dd-state").text("Accepted");
                        break;
                    default:
                        $("#dd-state").text("Unknown");
                        break;
                }

                state = response.State;
                startDate = date;
                stayCount = response.OvernightStayCount;
            })
            .catch(function (response) {
                if (Object.prototype.toString.call(response) == '[object String]') {
                    if (response.indexOf("TypeError") < 0) {
                        alert(response);
                    }
                }
            })
    }
    else {
        location.href = "/";
    }
};


$(document).ready(function () {
    getRole().then(function () {
        let urlParts = location.href.split("/");
        localStorage.setItem("reservationId", urlParts[urlParts.length - 1]);

        getReservationDetails(localStorage.getItem("reservationId")).then(function () {
            let role = localStorage.getItem("role");
            let today = new Date();

            switch (role) {
                case "0":
                    break;
                case "1":
                    if (today > startDate) {
                        if (state === 3) {
                            $("#btn-complete").show();
                        }
                    }
                    else {
                        if (state === 0) {
                            $("#btn-accept").show();
                            $("#btn-decline").show();
                        }
                        else if (state === 3) {
                            $("#btn-decline").show();
                        }
                    }

                    break;
                case "2":
                    console.log(today)
                    console.log(startDate);
                    if (today <= startDate) {
                        if (state === 0 || state === 3) {
                            $("#btn-cancel").show();
                        }
                    }
                    break;
                default:
                    location.href = "/";
                    break;
            }
        });
    });
});

let setState = function (state) {
    let url = "/api/Reservations/SetState";

    return fetch(url, {
        method: "POST",
        headers: {
            "Authorization": "Bearer " + localStorage.getItem("token"),
            "Content-Type": "text/plain",
            "New-State": state,
            "Reservation-Id": localStorage.getItem("reservationId")
        }
    }).then(function () {
        location.reload();
    }).catch(function (response) {
        alert(response);
    });
}

$(document).on("click", "#btn-decline", function () {
    setState(1);
});

$(document).on("click", "#btn-cancel", function () {
    setState(2);
});

$(document).on("click", "#btn-accept", function () {
    setState(3);
});
﻿//Shows all apartments and enables other buttons
//
let showApartments = function () {
    $("#list").empty();
    $("#users-search").hide();
    $("#apartments-search").show();
    $("#apartments-filter").show();
    $("#btn-users").prop("disabled", false);
    $("#btn-apartments").prop("disabled", true);
    $("#btn-reservations").prop("disabled", false);
    getApartments();
}

//Shows all users and enables other buttons
//
let showUsers = function () {
    $("#list").empty();
    $("#apartments-search").hide();
    $("#apartments-filter").hide();
    $("#users-search").show();
    $("#btn-users").prop("disabled", true);
    $("#btn-apartments").prop("disabled", false);
    $("#btn-reservations").prop("disabled", false);
    getUsers();
}

//Shows all reservations and enables other buttons
//
let showReservations = function () {
    $("#list").empty();
    $("#btn-users").prop("disabled", false);
    $("#btn-apartments").prop("disabled", false);
    $("#btn-reservations").prop("disabled", true);
    getReservations();
}

let show = function () {
    let lastMenu = sessionStorage.getItem("lastSelectedMenu");

    switch (lastMenu) {
        case "users":
            showUsers();
            break;
        case "reservations":
            showReservations();
            break;
        default:
            showApartments();
            break;
    }
}

//Shows apartments when Users button is clicked
//
$(document).ready(function () {
    $(document).on("click", "#btn-apartments", function () {
        let role = localStorage.getItem("role");

        if (role == "0" || role === "1" || role === "2") {
            sessionStorage.setItem("lastSelectedMenu", "apartments");
            showApartments();
        }
    })
});

//Shows users when Users button is clicked
//
$(document).ready(function () {
    $(document).on("click", "#btn-users", function () {
        let role = localStorage.getItem("role");

        if (role == "0" || role === "1" || role === "2") {
            sessionStorage.setItem("lastSelectedMenu", "users");
            showUsers();
        }
    })
});

//Shows reservations when Users button is clicked
//
$(document).ready(function () {
    $(document).on("click", "#btn-reservations", function () {
        let role = localStorage.getItem("role");

        if (role == "0" || role === "1" || role === "2") {
            sessionStorage.setItem("lastSelectedMenu", "reservations");
            showReservations();
        }
    })
});

$(document).ready(function () {
    getRole().then(function () {
        let role = localStorage.getItem("role");

        if (role === "0" || role === "1") {
            $("#btn-reservations").text("Reservations");
            show();
            $("#div-active").show();
        }
        else if (role === "2") {
            $("#btn-reservations").text("My Reservations");
            show();
        }
        else {
            $("#div-menus").hide();
            getApartments();
            return;
        }
    });
});
﻿let usernameCondition = false;
let passwordCondition = false;
let passwordConfirmCondition = false;
let passwordMinLength = 6;

$(document).ready(function () {
    formFilled();
});

let formValid = function () {
    if (formFilled() &&
        usernameCondition &&
        passwordCondition &&
        passwordConfirmCondition)
        $("#btn-register").prop("disabled", false);
    else
        $("#btn-register").prop("disabled", true);
}

let formFilled = function () {
    if ($("#Username").val().length != 0 &&
        $("#Password").val().length != 0 &&
        $("#ConfirmPassword").val().length != 0 &&
        $("#Name").val().length != 0 &&
        $("#Surname").val().length != 0) {
        $("#text-fields-required").hide("fade");
        return true;
    }
    else {
        $("#text-fields-required").show("fade");
        return false;
    }
}

// Does the username already exist?
$(document).ready(function () {
    $(document).on("keyup", "#Username", function () {
        $.ajax({
            url: "/api/users/checkusername/",
            method: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({
                Username: $("#Username").val(),
                Password: $("#Password").val(),
                ConfirmPassword: $("#ConfirmPassword").val(),
                Name: $("#Name").val(),
                Surname: $("#Surname").val(),
                Gender: $("#Gender").val()
            }),
            success: function (response) {
                if (response) {
                    //username free
                    usernameCondition = true;
                    $("#text-username-taken").hide("fade");
                    formValid();
                }
                else {
                    //username taken
                    usernameCondition = false;
                    $("#btn-register").prop("disabled", true);
                    $("#text-username-taken").show("fade");
                }
            }
        }); 
    });
});

// Is password longer than 6?
$(document).ready(function () {
    $(document).on("keyup", "#Password", function () {
        if ($("#Password").val().length < passwordMinLength) {
            $("#btn-register").prop("disabled", true);
            $("#text-password-short").show("fade");
            passwordCondition = false;
        }
        else {
            $("#text-password-short").hide("fade");
            passwordCondition = true;
            formValid();
        }
    });
});

// Is password confirmation correct?
function testPasswords() {
    if ($("#Password").val() === $("#ConfirmPassword").val()) {
        $("#text-password-different").hide("fade");
        passwordConfirmCondition = true;
        formValid();
    }
    else {
        $("#btn-register").prop("disabled", true);
        $("#text-password-different").show("fade");
        passwordConfirmCondition = false;
    }
}

$(document).ready(function () {
    $(document).on("keyup", "#ConfirmPassword", function () {
        testPasswords();
    });
    $(document).on("keyup", "#Password", function () {
        testPasswords();
    });
});


$(document).ready(function () {
    $(document).on("keyup", ".form-control", function () {
        formValid();
    });
});

$(document).ready(function () {
    $(document).on("click", "#btn-register", function () {
        $.ajax({
            url: "/api/users/register/",
            method: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({
                Username: $("#Username").val(),
                Password: $("#Password").val(),
                ConfirmPassword: $("#ConfirmPassword").val(),
                Name: $("#Name").val(),
                Surname: $("#Surname").val(),
                Gender: $("#Gender").val()
            }),
            success: function () {
                window.location.href = "/home/login";
            },
            error: function (response) {
                $("#text-error").text(response.statusText);
                $("#div-error").show("fade");
            }
        });
    });
});


﻿using ApartmentsAPI.Models;
using ApartmentsAPI.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ApartmentsAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            string path = HttpContext.Current.Server.MapPath(@"~/App_Data/admins.txt");
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs);
            string raw = sr.ReadToEnd();

            List<User> admins = new List<User>();

            try
            {
                admins = Json.Decode<List<User>>(raw);
                ApartmentsDbContext db = new ApartmentsDbContext();

                admins.ForEach(admin =>
                {
                    admin.Blocked = false;
                    if (!db.Users.Any(a => a.Username == admin.Username))
                    {
                        db.Users.Add(admin);
                    }
                });

                db.SaveChanges();
            }
            catch(Exception e)
            {
                throw new Exception("Error reading Admin data from: " + path + "\nException message: " + e.Message);
            }

            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            GlobalConfiguration.Configuration.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}

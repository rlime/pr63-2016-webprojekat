﻿namespace ApartmentsAPI
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Apartments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        RoomCount = c.Int(nullable: false),
                        GuestCount = c.Int(nullable: false),
                        PricePerNight = c.Double(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CheckInTime_Id = c.Int(),
                        CheckOutTime_Id = c.Int(),
                        Host_Id = c.Int(),
                        Location_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RentDateTimes", t => t.CheckInTime_Id)
                .ForeignKey("dbo.RentDateTimes", t => t.CheckOutTime_Id)
                .ForeignKey("dbo.Users", t => t.Host_Id)
                .ForeignKey("dbo.Locations", t => t.Location_Id)
                .Index(t => t.CheckInTime_Id)
                .Index(t => t.CheckOutTime_Id)
                .Index(t => t.Host_Id)
                .Index(t => t.Location_Id);
            
            CreateTable(
                "dbo.Amenities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 2147483647),
                        Apartment_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Apartments", t => t.Apartment_Id)
                .Index(t => t.Apartment_Id);
            
            CreateTable(
                "dbo.RentDateTimes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Apartment_Id = c.Int(),
                        Apartment_Id1 = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Apartments", t => t.Apartment_Id)
                .ForeignKey("dbo.Apartments", t => t.Apartment_Id1)
                .Index(t => t.Apartment_Id)
                .Index(t => t.Apartment_Id1);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(maxLength: 2147483647),
                        Rating = c.Int(nullable: false),
                        Apartment_Id = c.Int(),
                        Guest_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Apartments", t => t.Apartment_Id)
                .ForeignKey("dbo.Users", t => t.Guest_Id)
                .Index(t => t.Apartment_Id)
                .Index(t => t.Guest_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(nullable: false, maxLength: 2147483647),
                        Password = c.String(nullable: false, maxLength: 2147483647),
                        Name = c.String(nullable: false, maxLength: 2147483647),
                        Surname = c.String(nullable: false, maxLength: 2147483647),
                        Blocked = c.Boolean(nullable: false),
                        Gender = c.Int(nullable: false),
                        Role = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Photos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Path = c.String(maxLength: 2147483647),
                        Apartment_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Apartments", t => t.Apartment_Id)
                .Index(t => t.Apartment_Id);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                        Address = c.String(maxLength: 2147483647),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Reservations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OvernightStayCount = c.Int(nullable: false),
                        State = c.Int(nullable: false),
                        Apartment_Id = c.Int(),
                        Guest_Id = c.Int(),
                        StartDate_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Apartments", t => t.Apartment_Id)
                .ForeignKey("dbo.Users", t => t.Guest_Id)
                .ForeignKey("dbo.RentDateTimes", t => t.StartDate_Id)
                .Index(t => t.Apartment_Id)
                .Index(t => t.Guest_Id)
                .Index(t => t.StartDate_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reservations", "StartDate_Id", "dbo.RentDateTimes");
            DropForeignKey("dbo.Reservations", "Guest_Id", "dbo.Users");
            DropForeignKey("dbo.Reservations", "Apartment_Id", "dbo.Apartments");
            DropForeignKey("dbo.RentDateTimes", "Apartment_Id1", "dbo.Apartments");
            DropForeignKey("dbo.Apartments", "Location_Id", "dbo.Locations");
            DropForeignKey("dbo.Photos", "Apartment_Id", "dbo.Apartments");
            DropForeignKey("dbo.Apartments", "Host_Id", "dbo.Users");
            DropForeignKey("dbo.Comments", "Guest_Id", "dbo.Users");
            DropForeignKey("dbo.Comments", "Apartment_Id", "dbo.Apartments");
            DropForeignKey("dbo.Apartments", "CheckOutTime_Id", "dbo.RentDateTimes");
            DropForeignKey("dbo.Apartments", "CheckInTime_Id", "dbo.RentDateTimes");
            DropForeignKey("dbo.RentDateTimes", "Apartment_Id", "dbo.Apartments");
            DropForeignKey("dbo.Amenities", "Apartment_Id", "dbo.Apartments");
            DropIndex("dbo.Reservations", new[] { "StartDate_Id" });
            DropIndex("dbo.Reservations", new[] { "Guest_Id" });
            DropIndex("dbo.Reservations", new[] { "Apartment_Id" });
            DropIndex("dbo.Photos", new[] { "Apartment_Id" });
            DropIndex("dbo.Comments", new[] { "Guest_Id" });
            DropIndex("dbo.Comments", new[] { "Apartment_Id" });
            DropIndex("dbo.RentDateTimes", new[] { "Apartment_Id1" });
            DropIndex("dbo.RentDateTimes", new[] { "Apartment_Id" });
            DropIndex("dbo.Amenities", new[] { "Apartment_Id" });
            DropIndex("dbo.Apartments", new[] { "Location_Id" });
            DropIndex("dbo.Apartments", new[] { "Host_Id" });
            DropIndex("dbo.Apartments", new[] { "CheckOutTime_Id" });
            DropIndex("dbo.Apartments", new[] { "CheckInTime_Id" });
            DropTable("dbo.Reservations");
            DropTable("dbo.Locations");
            DropTable("dbo.Photos");
            DropTable("dbo.Users");
            DropTable("dbo.Comments");
            DropTable("dbo.RentDateTimes");
            DropTable("dbo.Amenities");
            DropTable("dbo.Apartments");
        }
    }
}

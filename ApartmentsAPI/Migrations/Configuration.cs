namespace ApartmentsAPI
{
    using ApartmentsAPI.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Data.Entity.Migrations.Sql;
    using System.Data.SQLite.EF6.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<ApartmentsAPI.ApartmentsDbContext>
    {
        public Configuration()
        {
            SetSqlGenerator("System.Data.SQLite", new SQLiteMigrationSqlGenerator());
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(ApartmentsAPI.ApartmentsDbContext context)
        {
            //  This method will be called after migrating to the latest version.
            return;

            #region SEED
            var users = new List<User>()
            {
                new User()
                {
                    Gender = EGender.Female,
                    Name = "Sanela",
                    Surname = "Halilovic",
                    Username = "sassja",
                    Password = "fmjam",
                    Role = ERole.Guest,
                    Blocked = false
                },
                new User()
                {
                    Gender = EGender.Male,
                    Name = "Vukasin",
                    Surname = "Jasnic",
                    Username = "marlon",
                    Password = "favela",
                    Role = ERole.Guest,
                    Blocked = false
                },
                new User()
                {
                    Gender = EGender.Female,
                    Name = "Milena",
                    Surname = "Jankovic",
                    Username = "mimi",
                    Password = "kabasti",
                    Role = ERole.Host,
                    Blocked = false
                },
                new User()
                {
                    Gender = EGender.Male,
                    Name = "Nikola",
                    Surname = "Jelic",
                    Username = "mikri",
                    Password = "romale",
                    Role = ERole.Host,
                    Blocked = false
                }
            };

            users.ForEach(user => context.Users.AddOrUpdate(user));
            context.SaveChanges();

            List<Apartment> apartments = new List<Apartment>() { new Apartment(), new Apartment(), new Apartment() };
            apartments[0].GuestCount = 3;
            apartments[0].IsActive = true;
            apartments[0].PricePerNight = 30;
            apartments[0].RoomCount = 3;
            apartments[0].Type = EApartmentType.Full;
            apartments[0].Host = users[2];

            apartments[1].GuestCount = 2;
            apartments[1].IsActive = true;
            apartments[1].PricePerNight = 25;
            apartments[1].RoomCount = 1;
            apartments[1].Type = EApartmentType.Room;
            apartments[1].Host = users[2];

            apartments[2].GuestCount = 3;
            apartments[2].IsActive = false;
            apartments[2].PricePerNight = 35;
            apartments[2].RoomCount = 2;
            apartments[2].Type = EApartmentType.Full;
            apartments[2].Host = users[2];

            List<string> addresses = new List<string>()
            {
                "Svetog Save 3 Sombor 25000 Srbija",
                "Pavla Papa 1 Novi Sad 21000 Srbija",
                "Rade Koncara 17 Apatin 25260 Srbija"
            };

            List<Location> locations = new List<Location>()
            {
                new Location(45.7677211, 19.1269815, addresses[0]),
                new Location(45.2545314, 19.8413098, addresses[1]),
                new Location(45.6794552, 18.9865851, addresses[2])
            };

            List<Comment> comments = new List<Comment>()
            {
                new Comment(users[0], apartments[0], "Dao bih maksimalnu ocenu, ali zavese su bile prljave!", 4),
                new Comment(users[1], apartments[1], "Sve cakum pakum! Svaka cast!", 5)
            };

            List<Amenity> amenities = new List<Amenity>()
            {
                new Amenity("Air Conditioning"),
                new Amenity("Elevator"),
                new Amenity("TV"),
                new Amenity("WiFi"),
                new Amenity("Washer"),
                new Amenity("Microwave"),
                new Amenity("Kitchen")
            };

            List<Photo> photos = new List<Photo>()
            {
                new Photo("./photo1.jpg"),
                new Photo("./photo2.jpg"),
                new Photo("./photo3.jpg"),
                new Photo("./photo4.jpg")
            };

            List<RentDateTime> dates = new List<RentDateTime>();

            for (int i = 0; i < 15; i++)
            {
                dates.Add(new RentDateTime(DateTime.Now.AddDays(i)));
            }

            List<Reservation> reservations = new List<Reservation>()
            {
                new Reservation(1, DateTime.Now.AddDays(2)),
                new Reservation(3, DateTime.Now.AddDays(3)),
                new Reservation(2, DateTime.Now.AddDays(7)),
                new Reservation(1, DateTime.Now.AddDays(3)),
                new Reservation(3, DateTime.Now.AddDays(1)),
                new Reservation(4, DateTime.Now.AddDays(5))
            };

            apartments[0].Amenities.Add(amenities[0]);
            apartments[0].Amenities.Add(amenities[1]);
            apartments[0].Amenities.Add(amenities[2]);

            apartments[1].Amenities.Add(amenities[3]);
            apartments[1].Amenities.Add(amenities[4]);
            apartments[1].Amenities.Add(amenities[5]);

            apartments[2].Amenities.Add(amenities[6]);
            apartments[2].Amenities.Add(amenities[1]);
            apartments[2].Amenities.Add(amenities[2]);

            apartments[0].RentDates.Add(dates[0]);
            apartments[0].RentDates.Add(dates[1]);
            apartments[0].RentDates.Add(dates[2]);
            apartments[0].RentDates.Add(dates[3]);
            apartments[0].RentDates.Add(dates[4]);

            apartments[1].RentDates.Add(dates[5]);
            apartments[1].RentDates.Add(dates[6]);
            apartments[1].RentDates.Add(dates[7]);
            apartments[1].RentDates.Add(dates[8]);
            apartments[1].RentDates.Add(dates[9]);

            apartments[2].RentDates.Add(dates[10]);
            apartments[2].RentDates.Add(dates[11]);
            apartments[2].RentDates.Add(dates[12]);
            apartments[2].RentDates.Add(dates[13]);
            apartments[2].RentDates.Add(dates[14]);

            apartments[0].Comments.Add(comments[0]);
            apartments[1].Comments.Add(comments[1]);

            apartments[0].Images.Add(photos[0]);
            apartments[0].Images.Add(photos[1]);
            apartments[1].Images.Add(photos[2]);
            apartments[1].Images.Add(photos[3]);

            apartments[0].Location = locations[0];
            apartments[1].Location = locations[1];
            apartments[2].Location = locations[2];

            apartments[0].Reservations.Add(reservations[0]);
            apartments[0].Reservations.Add(reservations[1]);
            apartments[0].Reservations.Add(reservations[2]);
            apartments[1].Reservations.Add(reservations[3]);
            apartments[1].Reservations.Add(reservations[4]);
            apartments[2].Reservations.Add(reservations[5]);

            apartments[0].CheckInTime = new RentDateTime(DateTime.Now);
            apartments[1].CheckInTime = new RentDateTime(DateTime.Now);
            apartments[2].CheckInTime = new RentDateTime(DateTime.Now);

            apartments[0].CheckOutTime = new RentDateTime(DateTime.Now);
            apartments[1].CheckOutTime = new RentDateTime(DateTime.Now);
            apartments[2].CheckOutTime = new RentDateTime(DateTime.Now);

            apartments.ForEach(apartment => context.Apartments.AddOrUpdate(apartment));

            context.SaveChanges();
            #endregion


            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}

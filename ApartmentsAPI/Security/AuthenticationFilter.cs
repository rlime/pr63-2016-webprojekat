﻿using ApartmentsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace ApartmentsAPI.Security
{
    public class AuthenticationFilter : ActionFilterAttribute
    {
        private static List<string> Exclusions = new List<string>()
        {
            "Register",
            "CheckUsername",
            "Login",
            "GetApartments",
            "GetApartment",
            "GetAmenities",
            "GetUser",
            "GetDetails",
            "GetRole",
            "GetReservation"
        };

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (Exclusions.Contains(actionContext.ActionDescriptor.ActionName))
            {
                return;
            }

            if (actionContext.Request.Headers.Authorization == null)
            {
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);

            }
            else
            {
                string token = actionContext.Request.Headers.Authorization.Parameter;

                if (!AuthorizationTools.VerifyToken(token))
                {
                    actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                }
            }
        }
    }
}
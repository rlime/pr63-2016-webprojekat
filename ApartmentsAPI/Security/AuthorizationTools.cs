﻿using ApartmentsAPI.Models;
using ApartmentsAPI.Security.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;

namespace ApartmentsAPI.Security
{
    public static class AuthorizationTools
    {
        private static readonly string secret = "j5mgi7M8gol9ES9fTaEV";

        public static string GenerateToken(User user)
        {
            Header header = new Header();
            Payload payload = new Payload(
                user.Id,
                user.Username,
                user.Name,
                user.Surname,
                user.Gender,
                user.Role,
                user.Blocked
            );

            Signature signature = new Signature(header, payload, secret);
            JWT jwt = new JWT(header, payload, signature);

            string signed = Sign(jwt);

            VerifyToken(CreateJWTString(header, payload, signed));

            return CreateJWTString(header, payload, signed);
        }

        private static string CreateJWTString(Header header, Payload payload, string signed)
        {
            string jsonHeader = Json.Encode(header);
            string jsonPayload = Json.Encode(payload);
            string jsonSigned = Json.Encode(signed);

            return $"{Encoder.Base64Encode(jsonHeader)}.{Encoder.Base64Encode(jsonPayload)}.{Encoder.Base64Encode(jsonSigned)}"; 
        }

        // Always uses SHA256
        private static string Sign(JWT jwt)
        {
            return Encoder.GetSHA256(jwt.ToString());
        }

        public static bool VerifyToken(string token)
        {
            if(token == null)
            {
                return false;
            }

            string[] parts = token.Split('.');

            if(parts.Length != 3)
            {
                return false;
            }

            string jsonHeader = string.Empty;
            string jsonPayload = string.Empty;
            string jsonSigned = string.Empty;

            try
            {
                jsonHeader = (string)Encoder.Base64Decode(parts[0]);
                jsonPayload = (string)Encoder.Base64Decode(parts[1]);
                jsonSigned = (string)Encoder.Base64Decode(parts[2]);
            }
            catch
            {
                return false;
            }

            if(jsonHeader == null || jsonPayload == null || jsonSigned == null)
            {
                return false;
            }

            // Removes quote signs
            string signature = jsonSigned.Substring(1, jsonSigned.Length - 2);

            Header header = null;
            Payload payload = null;

            try
            {
                header = Json.Decode<Header>(jsonHeader);
                payload = Json.Decode<Payload>(jsonPayload);
            }
            catch
            {
                return false;
            }

            Signature newSignature = new Signature(header, payload, secret);
            JWT newJWT = new JWT(header, payload, newSignature);
            string serverSignature = Sign(newJWT);

            if (serverSignature.Equals(signature))
            {
                return true;
            }

            return false;
        }

        public static int GetUserIdFromToken(string token)
        {
            string jsonPayload;
            Payload payload;

            try
            { 
                jsonPayload = (string)Encoder.Base64Decode(token.Split('.')[1]);
                payload = Json.Decode<Payload>(jsonPayload);

                return payload.Id;
            }
            catch
            {
                return -1;
            }
        }

        public static ERole GetUserRoleFromToken(string token)
        {
            string jsonPayload; 
            Payload payload;

            try
            {
                jsonPayload = (string)Encoder.Base64Decode(token.Split('.')[1]);
                payload = Json.Decode<Payload>(jsonPayload);
            }
            catch
            {
                throw new Exception("Bad token");
            }

            return payload.Role;
        }

        public static EAuthStatus AuthorizeToken(string token, ERole role)
        {
            if(token == null || token == string.Empty)
            {
                return EAuthStatus.NOT_AUTHENTICATED;
            }
            else
            {
                if (!VerifyToken(token))
                {
                    return EAuthStatus.NOT_AUTHENTICATED;
                }
                else
                {
                    if (GetUserRoleFromToken(token) != role)
                        return EAuthStatus.NOT_AUTHORIZED;
                    else
                        return EAuthStatus.AUTHORIZED;
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApartmentsAPI.Security.Model
{
    [Serializable]
    public class Signature
    {
        public string EncodedHeader { get; set; }
        public string EncodedPayload { get; set; }
        public string EncodedSecret { get; set; }
        
        public Signature(Header header, Payload payload, string secret)
        {
            EncodedHeader = Encoder.Base64Encode(header);
            EncodedPayload = Encoder.Base64Encode(payload);
            EncodedSecret = Encoder.Base64Encode(secret);
        }

        public override string ToString()
        {
            return $"Signature[encodedHeader:{EncodedHeader}, encodedPayload:{EncodedPayload}, encodedSecret:{EncodedSecret}]";
        }
    }
}
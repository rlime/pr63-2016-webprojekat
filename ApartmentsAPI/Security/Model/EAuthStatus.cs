﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApartmentsAPI.Security.Model
{
    public enum EAuthStatus
    {
        NOT_AUTHENTICATED = 0,
        NOT_AUTHORIZED,
        AUTHORIZED
    }
}
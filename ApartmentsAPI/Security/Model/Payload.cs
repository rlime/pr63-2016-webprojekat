﻿using ApartmentsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApartmentsAPI.Security.Model
{
    [Serializable]
    public class Payload
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public EGender Gender { get; set; }
        public ERole Role { get; set; }
        public bool Blocked { get; set; }

        public Payload() { }

        public Payload(int id, string username, string name, string surname, EGender gender, ERole role, bool blocked)
        {
            Id = id;
            Username = username;
            Name = name;
            Surname = surname;
            Gender = gender;
            Role = role;
            Blocked = blocked;
        }

        public override string ToString()
        {
            return $"Payload[id:{Id}, username:{Username}, name:{Name}, surname:{Surname}, role:{Role}, blocked:{Blocked.ToString()}]";
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApartmentsAPI.Security.Model
{
    [Serializable]
    public class JWT
    {
        public Header Header { get; set; }
        public Payload Payload { get; set; }
        public Signature Signature { get; set; }

        public JWT(Header header, Payload payload, Signature signature)
        {
            Header = header;
            Payload = payload;
            Signature = signature;
        }

        public override string ToString()
        {
            return $"JWT[header:{Header}, payload:{Payload}, signature:{Signature}]";
        }
    }
}
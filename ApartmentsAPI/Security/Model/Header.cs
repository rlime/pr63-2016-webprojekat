﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApartmentsAPI.Security.Model
{
    [Serializable]
    public class Header
    {
        private string algorithm = "SHA256";
        private string type = "JWT";

        public string Algorithm { get { return algorithm; } }
        public string Type { get { return type; } }

        public Header() { }

        public override string ToString()
        {
            return $"Header[algorithm:{algorithm}, type:{type}]";
        }
    }
}
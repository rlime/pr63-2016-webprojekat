﻿using ApartmentsAPI;
using ApartmentsAPI.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Results;
using System.Web.Mvc;

namespace ApartmentsProject.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Register()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult UserDetails()
        {
            return View();
        }
        public ActionResult NewApartment()
        {
            return View();
        }
        public ActionResult UpdateAmenities()
        {
            return View();
        }
        public ActionResult ApartmentDetails()
        {
            return View();
        }

        public ActionResult ReservationDetails()
        {
            return View();
        }
    }
}

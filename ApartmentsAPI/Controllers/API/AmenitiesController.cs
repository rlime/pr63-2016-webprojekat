﻿using ApartmentsAPI.Models;
using ApartmentsAPI.Security;
using ApartmentsAPI.Security.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace ApartmentsAPI.Controllers.API
{
    [AuthenticationFilter]
    public class AmenitiesController : ApiController
    {

        private ApartmentsDbContext db = new ApartmentsDbContext();

        [NonAction]
        public static List<string> ReadAmenities()
        {
            string path = HttpContext.Current.Server.MapPath(@"~/App_Data/amenities.JSON");
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs);
            string raw = sr.ReadToEnd();

            List<string> amenityNames = new List<string>();

            try
            {
                amenityNames = System.Web.Helpers.Json.Decode<List<string>>(raw);
            }
            catch
            { }

            sr.Close();
            fs.Close();

            return amenityNames;
        }

        [HttpGet]
        [Route("api/Amenities/")]
        public IHttpActionResult GetAmenities()
        {
            List<string> amenityNames = ReadAmenities();

            return Ok(amenityNames);
        }

        [HttpPost]
        [Route("Amenities/Update")]
        public IHttpActionResult Update([FromBody] IEnumerable<string> Amenities)
        {
            if (AuthorizationTools.AuthorizeToken(ActionContext.Request.Headers.Authorization.Parameter, ERole.Admin) != EAuthStatus.AUTHORIZED)
            {
                return Unauthorized();
            }

            List<string> amenities = Amenities.ToList();

            for(int i = 0; i < amenities.Count(); i++)
            {
                amenities[i] = amenities[i].Trim(' ', '\n');
            }
            
            string path = HttpContext.Current.Server.MapPath(@"~/App_Data/amenities.JSON");
            FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);

            try
            {
                string json = System.Web.Helpers.Json.Encode(amenities);
                sw.Write(json);
            }
            catch
            {
                return InternalServerError();
            }

            sw.Close();
            fs.Close();

            return Ok();
        }
    }
}

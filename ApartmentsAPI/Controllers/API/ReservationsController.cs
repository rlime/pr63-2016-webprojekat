﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ApartmentsAPI;
using ApartmentsAPI.Models;
using ApartmentsAPI.Security;
using ApartmentsAPI.Security.Model;
using ApartmentsAPI.ViewModels;

namespace ApartmentsAPI.Controllers.API
{
    [AuthenticationFilter]
    public class ReservationsController : ApiController
    {
        private ApartmentsDbContext db = new ApartmentsDbContext();

        // GET: api/Reservations
        public IHttpActionResult GetReservations()
        {
            string token = string.Empty;

            try
            {
                token = ActionContext.Request.Headers.Authorization.Parameter;
            }
            catch { }

            int id;

            if((id = AuthorizationTools.GetUserIdFromToken(token)) < 0)
            {
                return Unauthorized();
            }

            ERole role = AuthorizationTools.GetUserRoleFromToken(token);
            List<Reservation> reservations = new List<Reservation>();

            if (role == ERole.Admin)
            {
                reservations = db.Reservations
                    .Include("Guest")
                    .Include("Apartment.Location")
                    .Include("StartDate").ToList();
            }
            else if (role == ERole.Host)
            {
                reservations = db.Reservations
                    .Include("Guest")
                    .Include("Apartment.Location")
                    .Include("StartDate").Where(r => r.Apartment.Host.Id == id).ToList();
            }
            else
            {
                reservations = db.Reservations
                    .Include("Guest")
                    .Include("Apartment.Location")
                    .Include("StartDate").Where(r => r.Guest.Id == id).ToList();
            }

            return Ok(reservations);

        }

        // GET: api/Reservations/5
        [ResponseType(typeof(Reservation))]
        public IHttpActionResult GetReservation(int id)
        {
            Reservation reservation;

            try
            {
                reservation = db.Reservations.Where(r => r.Id == id)
                    .Include("Apartment.Location")
                    .Include("StartDate")
                    .Include("Guest").First(); 

                return Ok(reservation);
            }
            catch
            {
                return BadRequest("Error finding apartment");
            }
        }

        // PUT: api/Reservations/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutReservation(int id, Reservation reservation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != reservation.Id)
            {
                return BadRequest();
            }

            db.Entry(reservation).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReservationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        
        [HttpPost]
        [Route("Reservations/Add")]
        public IHttpActionResult PostReservation([FromBody] string data)
        {
            string token = string.Empty;

            try
            {
                token = ActionContext.Request.Headers.Authorization.Parameter;
            }
            catch { }

            if (AuthorizationTools.AuthorizeToken(token, ERole.Guest) != EAuthStatus.AUTHORIZED)
            {
                return Unauthorized();
            }

            int id = AuthorizationTools.GetUserIdFromToken(token);

            List<string> splitData = data.Split(',').ToList();
            int apartmentId = -1;

            try
            {
                apartmentId = Int32.Parse(splitData[0]);
            }
            catch
            {
                return BadRequest("Error parsing apartment ID");
            }

            if(apartmentId == -1)
            {
                return BadRequest("No dates were given");
            }

            splitData.RemoveAt(0);

            if (splitData.Count == 0)
            {
                return BadRequest("No dates were given");
            }

            Apartment apartment = null;

            try
            {
                apartment = db.Apartments.Where(a => a.Id == apartmentId)
                   .Include("Reservations.StartDate")
                   .Include("RentDates").First();
            }
            catch
            {
                return BadRequest("Error finding apartment");
            }

            if (apartment == null)
            {
                return BadRequest("Bad apartment ID");
            }

            bool dateUnavailable = false, badDate = false;
            int stayCount = splitData.Count;

            List<DateTime> dateTimes = new List<DateTime>();
            CultureInfo provider = CultureInfo.InvariantCulture;

            splitData.ForEach(d =>
            {
                DateTime tmp;

                try
                {
                    tmp = DateTime.Parse(d);
                }
                catch
                {
                    badDate = true;
                    return;
                }

                dateTimes.Add(tmp);

                foreach (RentDateTime ad in apartment.GetAvailableDates())
                {
                    if (ad.Date.Date.Equals(tmp.Date))
                    {
                        dateUnavailable = false;
                        break;
                    }

                    dateUnavailable = true;
                };
            });

            if (dateUnavailable)
            {
                return BadRequest("Some dates are unavailable");
            }
            if (badDate)
            {
                return BadRequest("Error parsing dates");
            }

            apartment.RentDates = new List<RentDateTime>(apartment.AvailableDates);
            Reservation reservation = new Reservation(stayCount, dateTimes.Min());
            reservation.Apartment = apartment;
            reservation.Guest = db.Users.First(u => u.Id == id);
            apartment.Reservations.Add(reservation);
            db.SaveChanges();

            return Ok();
        }

        [HttpPost]
        [Route("api/Reservations/SetState")]
        public IHttpActionResult SetState()
        {
            string token = string.Empty;

            try
            {
                token = ActionContext.Request.Headers.Authorization.Parameter;
            }
            catch { }

            if (!AuthorizationTools.VerifyToken(token))
            {
                return Unauthorized();
            }

            string reservationIdString = Request.Headers.GetValues("Reservation-Id").First();
            string newStateString = Request.Headers.GetValues("New-State").First();

            int reservationId;
            int newStateInt;

            if(!Int32.TryParse(reservationIdString, out reservationId))
            {
                return BadRequest("Bad reservation ID");
            }

            if(!Int32.TryParse(newStateString, out newStateInt))
            {
                return BadRequest("Unknown state");
            }

            EReservationState newState = (EReservationState)newStateInt;
            Reservation reservation;

            try
            {
                reservation = db.Reservations.First(r => r.Id == reservationId);
            }
            catch
            {
                return BadRequest("Couldn't find Reservation");
            }

            reservation.State = newState;
            db.SaveChanges();

            return Ok();
        }

        // DELETE: api/Reservations/5
        [ResponseType(typeof(Reservation))]
        public IHttpActionResult DeleteReservation(int id)
        {
            Reservation reservation = db.Reservations.Find(id);
            if (reservation == null)
            {
                return NotFound();
            }

            db.Reservations.Remove(reservation);
            db.SaveChanges();

            return Ok(reservation);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ReservationExists(int id)
        {
            return db.Reservations.Count(e => e.Id == id) > 0;
        }
    }
}
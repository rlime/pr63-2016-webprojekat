﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;

namespace ApartmentsAPI.Controllers.API
{
    public class ImageController : ApiController
    {
        private static readonly IDictionary<int, string> _images = new Dictionary<int, string>();
        private static readonly IDictionary<string, string> _extensions =
            new Dictionary<string, string>()
        {
        { ".gif", "image/gif" },
        { ".jpg", "image/jpeg" },
        { ".jpeg", "image/jpeg" },
        { ".png", "image/png" },
        { ".svg", "image/svg+xml" }
        };

        [HttpGet]
        [Route("Image/Get")]
        public HttpResponseMessage GetImage(string imageName)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);

            string path = "~/Images/" + imageName;
            path = HostingEnvironment.MapPath(path);
            string extension = Path.GetExtension(path);
            try
            {
                byte[] contents = File.ReadAllBytes(path);

                MemoryStream ms = new MemoryStream(contents);

                response.Content = new StreamContent(ms);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/" + extension);

                return response;
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        [Route("Image/Upload")]
        public async Task<HttpResponseMessage> PostImage()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/Images/");
            var provider = new MultipartFormDataStreamProvider(root);

            List<string> filenames = new List<string>();
            List<string> extensions = new List<string>();

            try
            {
                // Read the form data.
                await Request.Content.ReadAsMultipartAsync(provider);

                // This illustrates how to get the file names.
                foreach (MultipartFileData file in provider.FileData)
                {
                    filenames.Add(file.LocalFileName.Split('\\').Last());
                    extensions.Add(file.Headers.ContentDisposition.FileName.Trim('\"').Split('.').Last());
                }

                string filename = string.Empty;

                for(int i = 0; i < filenames.Count; i++)
                {
                    filename = filenames[i] + "." + extensions[i];
                    File.Move(root + filenames[i], root + filename);
                    filenames[i] = filename;
                }

                return Request.CreateResponse(HttpStatusCode.OK, filenames);
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ApartmentsAPI;
using ApartmentsAPI.Models;
using ApartmentsAPI.Security;
using ApartmentsAPI.Security.Model;
using ApartmentsAPI.ViewModels;
using Newtonsoft.Json;

namespace ApartmentsAPI.Controllers.API
{
    [AuthenticationFilter]
    public class ApartmentsController : ApiController
    {
        private ApartmentsDbContext db = new ApartmentsDbContext();

        // GET: api/Apartments
        public IHttpActionResult GetApartments()
        {
            string token = string.Empty;

            try
            {
                token = ActionContext.Request.Headers.Authorization.Parameter;
            }
            catch { }

            ERole role;
            List<Apartment> apartments = new List<Apartment>();

            if (!AuthorizationTools.VerifyToken(token))
            {
                apartments = db.Apartments.Where(a => a.IsActive)
                    .Include("Location")
                    .Include("RentDates")
                    .Include("Reservations.StartDate")
                    .Include("Host")
                    .Include("Comments.Guest")
                    .Include("Images")
                    .Include("CheckInTime")
                    .Include("CheckOutTime")
                    .Include("Amenities").ToList();

                return Ok(apartments);
            }

            role = AuthorizationTools.GetUserRoleFromToken(token);

            if (role == ERole.Admin)
            {
                apartments = db.Apartments
                    .Include("Location")
                    .Include("RentDates")
                    .Include("Reservations.StartDate")
                    .Include("Host")
                    .Include("Comments.Guest")
                    .Include("Images")
                    .Include("CheckInTime")
                    .Include("CheckOutTime")
                    .Include("Amenities").ToList();

                return Ok(apartments);
            }
            else if (role == ERole.Host)
            {
                int id = AuthorizationTools.GetUserIdFromToken(token);
                apartments = db.Apartments.Where(a => a.Host.Id == id)
                    .Include("Location")
                    .Include("RentDates")
                    .Include("Reservations.StartDate")
                    .Include("Host")
                    .Include("Comments.Guest")
                    .Include("Images")
                    .Include("CheckInTime")
                    .Include("CheckOutTime")
                    .Include("Amenities").ToList();

                return Ok(apartments);
            }
            else
            {
                apartments = db.Apartments.Where(a => a.IsActive)
                    .Include("Location")
                    .Include("RentDates")
                    .Include("Reservations.StartDate")
                    .Include("Host")
                    .Include("Comments.Guest")
                    .Include("Images")
                    .Include("CheckInTime")
                    .Include("CheckOutTime")
                    .Include("Amenities").ToList();

                return Ok(apartments);
            }
        }

        // GET: api/Apartments/5
        [ResponseType(typeof(Apartment))]
        public IHttpActionResult GetApartment(int id)
        {
            Apartment apartment;
            try
            {
                apartment = db.Apartments.Where(a => a.Id == id)
                    .Include("Location")
                    .Include("RentDates")
                    .Include("Reservations.StartDate")
                    .Include("Host")
                    .Include("Comments.Guest")
                    .Include("Images")
                    .Include("CheckInTime")
                    .Include("CheckOutTime")
                    .Include("AvailableDates")
                    .Include("Amenities").First();

                apartment.GetAvailableDates();

                return Ok(apartment);
            }
            catch
            {
                return BadRequest("Error finding apartment");
            }
        }

        // PUT: api/Apartments/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutApartment(int id, Apartment apartment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != apartment.Id)
            {
                return BadRequest();
            }

            db.Entry(apartment).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApartmentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpPost]
        [Route("api/Apartments/Add")]
        public IHttpActionResult PostApartment(NewApartmentViewModel apartmentVM)
        {
            string token = string.Empty;

            try
            {
                token = ActionContext.Request.Headers.Authorization.Parameter;
            }
            catch { }

            if(AuthorizationTools.AuthorizeToken(token, ERole.Host) == EAuthStatus.AUTHORIZED)
            {
                Apartment apartment = new Apartment();
                apartment.Host = db.Users.Find(AuthorizationTools.GetUserIdFromToken(token));
                apartment.Type = apartmentVM.Type;
                apartment.RoomCount = apartmentVM.RoomCount;
                apartment.GuestCount = apartmentVM.GuestCount;
                apartment.Location = new Location(apartmentVM.Latitude, apartmentVM.Longitude, apartmentVM.Location);

                List<string> dates = apartmentVM.RentDates.Split(',').ToList();
                for(int i = 0; i < dates.Count; i++)
                {
                    dates[i] = dates[i].Trim(' ');

                    RentDateTime rdt = new RentDateTime();
                    rdt.Date = DateTime.Parse(dates[i]);
                    apartment.RentDates.Add(rdt);
                }

                apartmentVM.Photos.ForEach(p => apartment.Images.Add(new Photo(p)));
                apartment.PricePerNight = apartmentVM.PricePerNight;
                apartment.CheckInTime = new RentDateTime(DateTime.Parse(apartmentVM.CheckInTime));
                apartment.CheckOutTime = new RentDateTime(DateTime.Parse(apartmentVM.CheckOutTime));

                List<string> amenityNames = AmenitiesController.ReadAmenities();

                apartmentVM.Amenities.ForEach(a => apartment.Amenities.Add(new Amenity(a)));

                db.Apartments.Add(apartment);
                db.SaveChanges();

                return Ok();
            }

            return Unauthorized();
        }

        [HttpPut]
        [Route("api/Apartments/Edit")]
        public IHttpActionResult EditApartment(EditApartmentViewModel apartmentVM)
        {
            string token = ActionContext.Request.Headers.Authorization.Parameter;
            int apartmentId = apartmentVM.Id;
            int initiatorId = AuthorizationTools.GetUserIdFromToken(token);
            Apartment apartment = null;

            try
            {
                apartment = db.Apartments.Where(a => a.Id == apartmentId)
                    .Include("Host")
                    .Include("Images")
                    .Include("RentDates")
                    .Include("Amenities").First();
            }
            catch
            {
                return BadRequest("Error finding apartment");
            }

            if (apartment == null)
            {
                return BadRequest("Unknown apartment");
            }

            var errors = ModelState.Values.SelectMany(v => v.Errors);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (apartment.Host.Id != initiatorId && AuthorizationTools.AuthorizeToken(token, ERole.Admin) == EAuthStatus.NOT_AUTHORIZED)
            {
                return Unauthorized();
            }
            
            apartment.Type = apartmentVM.Type;
            apartment.RoomCount = apartmentVM.RoomCount;
            apartment.GuestCount = apartmentVM.GuestCount;
            apartment.Location = new Location(apartmentVM.Latitude, apartmentVM.Longitude, apartmentVM.Location);

            List<string> dates = apartmentVM.RentDates.Split(',').ToList();
            List<RentDateTime> rdts = new List<RentDateTime>();
            for (int i = 0; i < dates.Count; i++)
            {
                dates[i] = dates[i].Trim(' ');

                RentDateTime rdt = new RentDateTime();
                rdt.Date = DateTime.Parse(dates[i]);
                rdts.Add(rdt);
            }
            
            apartment.RentDates = MergeDates(apartment.RentDates.ToList(), rdts);
            apartment.Images = MergePhotos(apartment.Images.ToList(), apartmentVM.Photos);
            apartment.Amenities = MergeAmenities(apartment.Amenities.ToList(), apartmentVM.Amenities);

            apartment.PricePerNight = apartmentVM.PricePerNight;
            apartment.CheckInTime = new RentDateTime(DateTime.Parse(apartmentVM.CheckInTime));
            apartment.CheckOutTime = new RentDateTime(DateTime.Parse(apartmentVM.CheckOutTime));
            apartment.IsActive = apartmentVM.IsActive;
            
            db.SaveChanges();

            return Ok();
        }

        // DELETE: api/Apartments/5
        [ResponseType(typeof(Apartment))]
        public IHttpActionResult DeleteApartment(int id)
        {
            Apartment apartment = db.Apartments.Find(id);
            if (apartment == null)
            {
                return NotFound();
            }

            db.Apartments.Remove(apartment);
            db.SaveChanges();

            return Ok(apartment);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ApartmentExists(int id)
        {
            return db.Apartments.Count(e => e.Id == id) > 0;
        }

        private List<RentDateTime> MergeDates(List<RentDateTime> l1, List<RentDateTime> l2)
        {
            List<RentDateTime> retVal = l1;

            List<RentDateTime> ol1 = l1.OrderBy(r => r.Date).ToList();
            List<RentDateTime> ol2 = l2.OrderBy(r => r.Date).ToList();
            List<bool> isFound = new List<bool>();
            bool remove = true;
            int j;

            for (j = 0; j < ol2.Count; j++)
            {
                isFound.Add(false);
            }

            foreach (RentDateTime r1 in ol1)
            {
                remove = true;
                j = 0;

                foreach (RentDateTime r2 in ol2)
                {
                    if (r1.Date == r2.Date)
                    {
                        isFound[j] = true;
                        remove = false;
                        break;
                    }

                    j++;
                }

                if (remove)
                {
                    retVal.Remove(r1);
                }
            }

            for (int i = 0; i < isFound.Count; i++)
            {
                if (!isFound[i])
                {
                    retVal.Add(ol2[i]);
                }
            }

            return retVal;
        }

        private List<Photo> MergePhotos(List<Photo> l1, List<string> l2)
        {
            List<bool> isFound = new List<bool>();
            List<bool> isRemoved = new List<bool>();
            int i, j;

            for (i = 0; i < l2.Count; i++)
            {
                isFound.Add(false);
            }

            for (i = 0; i < l1.Count; i++)
            {
                isRemoved.Add(true);
            }

            i = 0;

            foreach (Photo p1 in l1)
            {
                j = 0;

                foreach (string p2 in l2)
                {
                    if (p1.Path == p2)
                    {
                        isFound[j] = true;
                        isRemoved[i] = false;
                        break;
                    }

                    j++;
                }

                i++;
            }

            for (i = 0; i < isRemoved.Count; i++)
            {
                if (isRemoved[i])
                {
                    l1.RemoveAt(i);
                    isRemoved.RemoveAt(i);
                    i--;
                }
            }

            for (i = 0; i < isFound.Count; i++)
            {
                if (!isFound[i])
                {
                    l1.Add(new Photo(l2[i]));
                }
            }

            return l1;
        }

        private List<Amenity> MergeAmenities(List<Amenity> l1, List<string> l2)
        {
            List<bool> isFound = new List<bool>();
            List<bool> isRemoved = new List<bool>();
            int i, j;

            for (i = 0; i < l2.Count; i++)
            {
                isFound.Add(false);
            }

            for (i = 0; i < l1.Count; i++)
            {
                isRemoved.Add(true);
            }

            i = 0;

            foreach (Amenity a1 in l1)
            {
                j = 0;

                foreach (string a2 in l2)
                {
                    if (a1.Name == a2)
                    {
                        isFound[j] = true;
                        isRemoved[i] = false;
                        break;
                    }

                    j++;
                }

                i++;
            }

            for (i = 0; i < isRemoved.Count; i++)
            {
                if (isRemoved[i])
                {
                    l1.RemoveAt(i);
                    isRemoved.RemoveAt(i);
                    i--;
                }
            }

            for (i = 0; i < isFound.Count; i++)
            {
                if (!isFound[i])
                {
                    l1.Add(new Amenity(l2[i]));
                }
            }

            return l1;
        }
    }
}
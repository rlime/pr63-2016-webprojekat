﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using ApartmentsAPI;
using ApartmentsAPI.Models;
using ApartmentsAPI.Security;
using ApartmentsAPI.Security.Model;
using ApartmentsAPI.ViewModels;

namespace ApartmentsAPI.Controllers
{
    [AuthenticationFilter]
    public class UsersController : ApiController
    {
        private ApartmentsDbContext db = new ApartmentsDbContext();

        // GET: api/Users
        public IHttpActionResult GetUsers()
        {
            string token = string.Empty;

            try
            {
                token = ActionContext.Request.Headers.Authorization.Parameter;
            }
            catch { }
            
            if (AuthorizationTools.VerifyToken(token))
            {
                return Ok(db.Users);
            }

            return Unauthorized();
        }

        //// GET: api/Users/5
        [ResponseType(typeof(User))]
        public IHttpActionResult GetUser(int id)
        {   
            User user = db.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        [HttpPost]
        [Route("api/Users/Register")]
        public IHttpActionResult Register(RegisterUserViewModel user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(db.Users.Any(u => u.Username.Equals(user.Username)))
            {
                return BadRequest("Username is taken");
            }

            if(user.Password.Length < 6)
            {
                return BadRequest("Password is too short");
            }

            if(user.Password != user.ConfirmPassword)
            {
                return BadRequest("Passwords do not match");
            }

            User tmp = new User
            {
                Username = user.Username,
                Password = user.Password,
                Name = user.Name,
                Surname = user.Surname,
                Gender = (EGender)user.Gender,
                Role = ERole.Guest,
                Blocked = false
            };

            db.Users.Add(tmp);
            db.SaveChanges();

            return Created("UsersApi", user);
        }

        [HttpPost]
        [Route("api/Users/Login")]
        public IHttpActionResult Login(LoginUserViewModel loginUser)
        {
            User user = null;

            try
            {
                user = db.Users.First(u => u.Username == loginUser.Username && u.Password == loginUser.Password);
            }
            catch
            {
                return BadRequest("Wrong username or password");
            }

            string token = AuthorizationTools.GenerateToken(user);

            return Ok(token);
        }

        [Route("api/Users/GetId")]
        public IHttpActionResult GetId()
        {
            int id = AuthorizationTools.GetUserIdFromToken(ActionContext.Request.Headers.Authorization.Parameter);

            if (id >= 0)
            {
                return Ok(id.ToString());
            }
            else
            {
                return BadRequest("Invalid token, you need to Log In!");
            }
        }

        [Route("api/Users/GetRole")]
        public IHttpActionResult GetRole()
        {
            int id = AuthorizationTools.GetUserIdFromToken(ActionContext.Request.Headers.Authorization.Parameter);

            try
            {
                ERole role = AuthorizationTools.GetUserRoleFromToken(ActionContext.Request.Headers.Authorization.Parameter);
                return Ok(role);
            }
            catch
            {
                return Ok(-1);
            }
        }

        [Route("api/Users/Details")]
        public IHttpActionResult GetDetails()
        {
            int id = AuthorizationTools.GetUserIdFromToken(ActionContext.Request.Headers.Authorization.Parameter);

            User user;

            try
            {
                user = db.Users.Find(id);
            }
            catch(Exception e)
            {
                return InternalServerError(e);
            }

            return Ok(user);
        }

        [HttpPut]
        [Route("api/Users/Edit")]
        public IHttpActionResult EditUser(EditUserViewModel userVM)
        {
            string token = ActionContext.Request.Headers.Authorization.Parameter;
            int userId = userVM.Id;
            int initiatorId = AuthorizationTools.GetUserIdFromToken(token);

            User user = db.Users.Find(userId);

            if (user == null)
            {
                return BadRequest("Unknown user");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(userId != initiatorId && AuthorizationTools.AuthorizeToken(token, ERole.Admin) == EAuthStatus.NOT_AUTHORIZED)
            {
                return Unauthorized();
            }

            if (user.Username != userVM.Username && db.Users.Any(u => u.Username.Equals(userVM.Username)))
            {
                return BadRequest("Username is taken");
            }

            if (userVM.Password.Length < 6 && userVM.Password != string.Empty)
            {
                return BadRequest("Password is too short");
            }

            if (userVM.Password != userVM.ConfirmPassword)
            {
                return BadRequest("Passwords do not match");
            }
            
            if(userVM.Role != ERole.Admin && user.Role == ERole.Admin)
            {
                return BadRequest("Can't change role of Admin");
            }
            else if (userVM.Role == ERole.Admin && user.Role != ERole.Admin)
            {
                return BadRequest("Can't set Admin role");
            }

            user.Username = userVM.Username;
            user.Name = userVM.Name;
            user.Surname = userVM.Surname;
            if (userVM.Password != string.Empty)
                user.Password = userVM.Password;
            user.Gender = userVM.Gender;
            user.Role = userVM.Role;
            

            db.SaveChanges();

            token = AuthorizationTools.GenerateToken(user);

            return Ok(token);
        }

        // DELETE: api/Users/5
        [ResponseType(typeof(User))]
        public IHttpActionResult DeleteUser(int id)
        {
            User user = db.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            db.SaveChanges();

            return Ok(user);
        }

        [HttpPost]
        [Route("api/Users/CheckUsername")]
        public IHttpActionResult CheckUsername(RegisterUserViewModel user)
        {
            string username = user.Username;

            if (username != null)
            {
                bool isValid = !db.Users.ToList().Exists(u => u.Username.Equals(username, StringComparison.CurrentCultureIgnoreCase));
                return Ok(isValid);
            }

            return Ok(true);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(int id)
        {
            return db.Users.Count(e => e.Id == id) > 0;
        }
    }
}